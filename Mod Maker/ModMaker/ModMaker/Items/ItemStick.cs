﻿using Amphyra.Items;

namespace Brent.Items
{
    public class ItemStick : Item
    {
        public ItemStick() : base("Stick", 6, ItemType.Normal, "Stick", 1)
        {

        }
    }
}
