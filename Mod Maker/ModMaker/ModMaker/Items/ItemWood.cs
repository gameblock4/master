﻿using Amphyra.Items;

namespace Brent.Items
{
    public class ItemWood : Item
    {
        public ItemWood(): base("Wood", 2, ItemType.Normal, "Wood", 2)
        {

        }
    }
}
