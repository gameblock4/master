﻿using Amphyra.Items;

namespace ModMaker.Items
{
    public class ItemBag : Item
    {
        public ItemBag() : base("Item bag", 19, ItemType.Normal, "ItemBag", 250, true, false)
        {
        }
    }
}
