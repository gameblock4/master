﻿using Amphyra.Items;

namespace Brent.Items
{
    public class ItemGold : Item
    {
        public ItemGold() : base("Gold", 4, ItemType.Normal, "Lot-131", 20)
        {

        }
    }
}
