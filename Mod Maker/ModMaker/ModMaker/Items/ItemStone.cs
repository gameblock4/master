﻿using Amphyra.Items;

namespace Brent.Items
{
    public class ItemStone : Item
    {
        public ItemStone() : base("Stone", 1, ItemType.Normal, "Stone", 2)
        {

        }
    }
}
