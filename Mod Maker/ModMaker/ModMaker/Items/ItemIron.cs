﻿using Amphyra.Items;

namespace Brent.Items
{
    public class ItemIron : Item
    {
        public ItemIron() : base("Iron ore", 3, ItemType.Normal, "ItemIronOre", 15)
        {

        }
    }
}
