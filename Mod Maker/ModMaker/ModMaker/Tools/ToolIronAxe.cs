﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Tools
{
    public class ToolIronAxe : AxeItem
    {
        public ToolIronAxe() : base("Iron axe", 9, "StoneAxe", 4, 1.5f)
        {

        }

        public override void OnUse(Actor usingActor, Resource collectedResource)
        {
            base.OnUse(usingActor, collectedResource);
            ParticleManager.InstantiateParticles(collectedResource.transform.position, ParticleType.meleeAttack);
        }
    }
}
