﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Tools
{
    public class ToolStonePickaxe : PickaxeItem
    {
        public ToolStonePickaxe() : base("Stone Pickaxe", 8, "IronPickaxe", 4, 3)
        {

        }

        public override void OnUse(Actor usingActor, Resource collectedResource)
        {
            base.OnUse(usingActor, collectedResource);
            ParticleManager.InstantiateParticles(collectedResource.transform.position, ParticleType.meleeAttack);
        }
    }
}
