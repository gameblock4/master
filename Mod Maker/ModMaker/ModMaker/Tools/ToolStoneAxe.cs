﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Tools
{
    public class ToolStoneAxe : AxeItem
    {
        public ToolStoneAxe() : base("Stone axe", 7, "StoneAxe", 4, 3)
        {

        }

        public override void OnUse(Actor usingActor, Resource collectedResource)
        {
            base.OnUse(usingActor, collectedResource);
            ParticleManager.InstantiateParticles(collectedResource.transform.position, ParticleType.meleeAttack);
        }
    }
}
