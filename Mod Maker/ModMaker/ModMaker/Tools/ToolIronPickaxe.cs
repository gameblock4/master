﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Tools
{
    public class ToolIronPickaxe : PickaxeItem
    {
        public ToolIronPickaxe() : base("Iron pickaxe", 10, "IronPickaxe", 4, 1.5f)
        {

        }

        public override void OnUse(Actor usingActor, Resource collectedResource)
        {
            base.OnUse(usingActor, collectedResource);
            ParticleManager.InstantiateParticles(collectedResource.transform.position, ParticleType.meleeAttack);
        }
    }
}
