﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Weapons
{
    public class WeaponStoneSword : AttackItem
    {
        public WeaponStoneSword() : base("Stone sword", 12, "Sword", 5, 3, 1.5f, AttackType.Slashing)
        {

        }

        public override void OnAttack(Actor attackedActor, Actor attackingActor)
        {
            base.OnAttack(attackedActor, attackingActor);
            ParticleManager.InstantiateParticles(attackedActor.transform.position, ParticleType.meleeAttack);
        }
    }
}
