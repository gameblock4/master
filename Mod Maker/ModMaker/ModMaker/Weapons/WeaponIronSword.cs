﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Weapons
{
    public class WeaponIronSword : AttackItem
    {
        public WeaponIronSword() : base("Iron sword", 13, "Sword", 35, 4, 1.0f, AttackType.Slashing)
        {

        }

        public override void OnAttack(Actor attackedActor, Actor attackingActor)
        {
            base.OnAttack(attackedActor, attackingActor);
            ParticleManager.InstantiateParticles(attackedActor.transform.position, ParticleType.meleeAttack);
        }
    }
}
