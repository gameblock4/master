﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.Weapons
{
    public class WeaponWoodenSword : AttackItem
    {
        public WeaponWoodenSword() : base("Wooden sword", 11, "Sword", 4, 2, 2.0f, AttackType.Slashing)
        {

        }

        public override void OnAttack(Actor attackedActor, Actor attackingActor)
        {
            base.OnAttack(attackedActor, attackingActor);
            ParticleManager.InstantiateParticles(attackedActor.transform.position, ParticleType.meleeAttack);
        }
    }
}
