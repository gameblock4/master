﻿using Amphyra.Items;

namespace ModMaker.ProtectiveItems.Armor
{
    public class ArmorBody : ArmorItem
    {
        public ArmorBody() : base("Iron Body", 17, "IronHelmet", 100, 20, ArmorType.Body)
        {
            AddProtectType(AttackType.Hacking);
            AddProtectType(AttackType.Slashing);
        }
    }
}
