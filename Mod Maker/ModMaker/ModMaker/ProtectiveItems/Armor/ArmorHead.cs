﻿using Amphyra.Items;

namespace ModMaker.ProtectiveItems.Armor
{
    public class ArmorHead : ArmorItem
    {
        public ArmorHead() : base("Iron helmet", 16, "IronHelmet", 50, 10, ArmorType.Helmet)
        {
            AddProtectType(AttackType.Hacking);
            AddProtectType(AttackType.Slashing);
        }
    }
}
