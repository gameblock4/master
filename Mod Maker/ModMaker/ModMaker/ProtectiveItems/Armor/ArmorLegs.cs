﻿using Amphyra.Items;

namespace ModMaker.ProtectiveItems.Armor
{
    public class ArmorLegs : ArmorItem
    {
        public ArmorLegs() : base("Iron Legs", 18, "IronHelmet", 70, 15, ArmorType.Leggings)
        {
            AddProtectType(AttackType.Hacking);
            AddProtectType(AttackType.Slashing);
        }
    }
}
