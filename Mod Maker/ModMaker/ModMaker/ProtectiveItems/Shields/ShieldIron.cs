﻿using Amphyra.Actors;
using Amphyra.Items;

namespace Brent.ProtectiveItems.Armor
{
    public class ShieldIron : ShieldItem
    {
        public ShieldIron() : base("Iron shield", 15, "WoodenShield", 8, 10)
        {
            AddProtectType(AttackType.Slashing);
            AddProtectType(AttackType.Hacking);
        }

        public override void OnBlock(Actor attackActor, Actor DefendActor)
        {
            base.OnBlock(attackActor, DefendActor);
            ParticleManager.InstantiateParticles(DefendActor.transform, attackActor.transform.position, ParticleType.block, 0);
        }
    }
}
