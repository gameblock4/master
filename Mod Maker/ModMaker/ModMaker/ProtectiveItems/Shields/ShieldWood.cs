﻿using Amphyra.Items;

namespace Brent.ProtectiveItems.Armor
{
    public class ShieldWood : ShieldItem
    {
        public ShieldWood() : base("Wooden shield", 14, "WoodenShield", 8, 5)
        {
            AddProtectType(AttackType.Slashing);
        }
    }
}
