﻿using UnityEngine;
using System.Collections;

/// <summary>
/// all attack types
/// </summary>
public enum AttackType
{
    Slashing = 1,
    Crushing = 2,
    Hacking = 4,
    Magic = 8,
}
