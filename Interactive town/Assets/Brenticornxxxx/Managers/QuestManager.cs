﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amphyra.Actors;

public class QuestManager : MonoBehaviour
{
    public Quest[] allQuests;
    public List<Quest> openQuests;

    public static QuestManager main;

    public QuestManager()
    {
        main = this;
    }

    public static bool CheckIfQuest(int id)
    {
        for (int i = 0; i < main.openQuests.Count; i++)
        {
            if (main.openQuests[i].itemIDToCollect == id)
            {
                return true;
            }
        }
        return false;
    }

    public static bool CheckIfQuest(string actor)
    {
        for (int i = 0; i < main.openQuests.Count; i++)
        {
            if (main.openQuests[i].ActorToKill == actor)
            {
                return true;
            }
        }
        return false;
    }

    public static void Collect(int id, int amount)
    {
        for (int i = 0; i < main.openQuests.Count; i++)
        {
            if (main.openQuests[i].itemIDToCollect == id)
            {
                main.openQuests[i].removeAmount(amount);
                if (main.openQuests[i].amount <= 0)
                {
                    Player.Main.inventory.AddToInventory(main.openQuests[i].itemReward, main.openQuests[i].rewardAmount);
                    main.openQuests.RemoveAt(i);
                }
            }
        }
    }

    public static void Collect(string actor, int amount)
    {
        for (int i = 0; i < main.openQuests.Count; i++)
        {
            if (main.openQuests[i].ActorToKill == actor)
            {
                main.openQuests[i].removeAmount(amount);
                if (main.openQuests[i].amount <= 0)
                {
                    Player.Main.CollectItem(main.openQuests[i].itemReward, main.openQuests[i].rewardAmount);
                    main.openQuests.RemoveAt(i);
                }
            }
        }
    }

    public static void StartQuest(int questNumber)
    {
        if (questNumber > 0 && questNumber < main.allQuests.Length)
        {
            main.openQuests.Add(main.allQuests[questNumber]);
        }
        else
            Debug.LogWarning("Quest with number \"" + questNumber + "\" couln't be found");
    }
}