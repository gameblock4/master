﻿using Amphyra.Town;
using UnityEngine;

public class NpcGoHomeState : NpcState
{
    private int foodLevel;

    private Building home;

    public NpcGoHomeState(NpcStateMachine stateMachine, Building home, int foodLevel = 0) : base(stateMachine)
    {
        Debug.DrawLine(stateMachineHolder.transform.position, home.transform.position);
        stateMachineHolder.pathFinding.SetDestination(home.transform.GetChild(0).position);
        this.foodLevel = foodLevel;
    }

    public override void OnBuildingEnter(Building building)
    {
        base.OnBuildingEnter(building);
        building.BuildingData.food += foodLevel;
        Debug.Log("food added to the building");
        stateMachine.ChangeState(new NpcGetFoodState(stateMachine, building));
    }
}
