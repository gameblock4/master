﻿using System;
using Amphyra.Actors;
using Amphyra.Town;
using UnityEngine;

public class NpcIdleState : NpcState
{
    public NpcIdleState(NpcStateMachine stateMachine) : base(stateMachine)
    {

    }

    public override void OnEnable()
    {
        Vector3 output;
        if (RandomPoint(stateMachineHolder.transform.position, 20, out output))
        {
            stateMachineHolder.pathFinding.SetDestination(output);
        }
        else Debug.Log(stateMachineHolder.transform.position);
        
    }

    public override void Update()
    {
        stateMachineHolder.NpcData.FoodLevel -= 1;
        if (stateMachineHolder.NpcData.FoodLevel < 5)
        {
            stateMachine.ChangeState(new NpcHungryState(stateMachine));
        }
    }
}
