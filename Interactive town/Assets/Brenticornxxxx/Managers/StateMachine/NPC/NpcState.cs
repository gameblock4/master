﻿using Amphyra.Actors.Npcs;
using Amphyra.Town;
using UnityEngine;

public abstract class NpcState
{
    public virtual void OnBuildingEnter(Building building) { }
    public virtual void OnBuildingExit(Building building) { }

    protected NpcStateMachine stateMachine;
    public Npc stateMachineHolder;

    public virtual void OnEnable() { }
    public virtual void Update() { stateMachineHolder.NpcData.FoodLevel--; }
    public virtual void OnDisable() { }

    public NpcState(NpcStateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
        stateMachineHolder = stateMachine.stateMachineHolder;
    }

    protected bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }

    public bool SetDestenation(Vector3 target)
    {
        stateMachineHolder.pathFinding.Resume();
        NavMeshHit edge;
        if (NavMesh.SamplePosition(target, out edge, 100, NavMesh.AllAreas))
        {
            return stateMachineHolder.pathFinding.SetDestination(edge.position);
        }
        return false;
    }
}
