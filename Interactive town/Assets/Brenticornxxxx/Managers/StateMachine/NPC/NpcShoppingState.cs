﻿using UnityEngine;
using System.Collections;
using Amphyra.Town;

public class NpcShoppingState : NpcState
{
    int foodLevel;
    private Building house;
    public NpcShoppingState(NpcStateMachine stateMachine, Building house) : base(stateMachine)
    {
        this.house = house;
        stateMachineHolder.LeaveBuilding();
    }

    public override void OnBuildingExit(Building building)
    {
        base.OnBuildingExit(building);
        stateMachine.ChangeState(new NpcGoHomeState(stateMachine, house, foodLevel));
    }

    public override void Update()
    {
        base.Update();
        if (stateMachineHolder.IsInBuilding)
        {
            foodLevel+= 25;
            Debug.Log(foodLevel);
        }
        if(foodLevel > 150)
        {
            stateMachineHolder.LeaveBuilding();
        }
    }
}
