﻿using UnityEngine;
using System.Collections;

public class NpcWalkingState : NpcState
{
    public NpcWalkingState(NpcStateMachine stateMachine, Vector3 posision) : base(stateMachine)
    {
        SetDestenation(posision);
    }

    public override void Update()
    {
        base.Update();
        if (!stateMachineHolder.pathFinding.pathPending)
        {
            if (stateMachineHolder.pathFinding.remainingDistance <= stateMachineHolder.pathFinding.stoppingDistance)
            {
                if (!stateMachineHolder.pathFinding.hasPath || stateMachineHolder.pathFinding.velocity.sqrMagnitude == 0f)
                {
                    stateMachine.ChangeState(new NpcIdleState(stateMachine));
                }
            }
        }
    }
}
