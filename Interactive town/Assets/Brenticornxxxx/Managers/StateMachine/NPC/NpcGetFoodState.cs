﻿using UnityEngine;
using Amphyra.Actors.Npcs;
using Amphyra.Town;

public class NpcGetFoodState : NpcState
{
    Building building;

    public NpcGetFoodState(NpcStateMachine stateMachine, Building building) : base(stateMachine)
    {
        this.building = building;
    }

    public override void Update()
    {
        base.Update();
        for (int i = 0; i < 8; i++)
        {
            if (building.BuildingData.food > 0)
            {
                if (stateMachineHolder.NpcData.FoodLevel < NpcStruct.maxFood)
                {
                    stateMachineHolder.NpcData.FoodLevel++;
                    building.BuildingData.food--;
                }
                else
                    stateMachineHolder.LeaveBuilding();
            }
            else
            {

            }
        }
    }

    public override void OnBuildingExit(Building building)
    {
        base.OnBuildingExit(building);
        Vector3 target;
        RandomPoint(stateMachineHolder.transform.position, 20, out target);
        stateMachine.ChangeState(new NpcWalkingState(stateMachine, target));
    }
}
