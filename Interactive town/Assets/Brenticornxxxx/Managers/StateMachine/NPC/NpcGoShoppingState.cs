﻿using UnityEngine;
using System.Collections;
using Amphyra.Town;

public class NpcGoShoppingState : NpcState
{
    private Building house;
    public NpcGoShoppingState(NpcStateMachine stateMachine, Building house) : base(stateMachine)
    {
        this.house = house;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        stateMachineHolder.LeaveBuilding();
        stateMachineHolder.pathFinding.SetDestination(TownManager.main.GetClosestBuilding(stateMachineHolder, BuildingType.Shop));
    }

    public override void OnBuildingEnter(Building building)
    {
        if (building.BuildingType != BuildingType.Shop)
        {
            stateMachineHolder.LeaveBuilding();
        }
        else stateMachine.ChangeState(new NpcShoppingState(stateMachine, house));
    }
}
