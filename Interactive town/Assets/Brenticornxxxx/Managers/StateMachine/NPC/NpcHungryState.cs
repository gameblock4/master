﻿using UnityEngine;
using System.Collections;
using Amphyra.Actors;
using Amphyra.Town;
using System;

public class NpcHungryState : NpcState
{
    public NpcHungryState(NpcStateMachine stateMachine) : base(stateMachine)
    {

    }

    public override void OnBuildingEnter(Building building)
    {
        stateMachine.ChangeState(new NpcGetFoodState(stateMachine, building));
    }

    public override void OnEnable()
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(stateMachineHolder.transform.position, out hit, 10, NavMesh.AllAreas))
        {
            stateMachineHolder.transform.position = hit.position;
        }
        NavMeshHit edge;
        if (NavMesh.SamplePosition(TownManager.main.GetClosestBuilding(stateMachineHolder, BuildingType.House), out edge, 100, NavMesh.AllAreas))
        {
            stateMachineHolder.pathFinding.SetDestination(edge.position);
        }
    }
}
