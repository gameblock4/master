﻿using UnityEngine;
using System.Collections;
using Amphyra.Actors.Npcs;

public class NpcStateMachine
{
    public NpcStateMachine(Npc stateMachineHolder, float updateDelay)
    {
        this.stateMachineHolder = stateMachineHolder;
        timerDelay = updateDelay;
        ChangeState(new NpcIdleState(this));
    }

    public NpcState currentstate;
    public NpcState lastState;
    public float timerDelay;
    public Npc stateMachineHolder;

    public bool isUpdateActive = true;

    public virtual void ChangeState(NpcState state)
    {
        state.stateMachineHolder = stateMachineHolder;
        if (currentstate != null)
            currentstate.OnDisable();
        lastState = currentstate;
        currentstate = state;
        currentstate.OnEnable();
    }

    public IEnumerator UpdateState()
    {
        while (isUpdateActive)
        {
            currentstate.Update();
            yield return new WaitForSeconds(timerDelay);
        }
    }
}
