﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleManager : MonoBehaviour
{
    /// <summary>list of move particles</summary>
    public List<GameObject> moveClick;
    /// <summary>list of hit particles</summary>
    public List<GameObject> hit;
    /// <summary>list of die particles</summary>
    public List<GameObject> die;
    /// <summary>list of spesialeffect particles</summary>
    public List<GameObject> specialEffects;
    /// <summary>list of block particles</summary>
    public List<GameObject> block;

    private static Dictionary<ParticleType, List<GameObject>> particles = new Dictionary<ParticleType, List<GameObject>>();

    public void Start()
    {
        particles.Add(ParticleType.movementClick, moveClick);
        particles.Add(ParticleType.meleeAttack, hit);
        particles.Add(ParticleType.die, die);
        particles.Add(ParticleType.specialEffect, specialEffects);
        particles.Add(ParticleType.block, block);
    }

    public static void InstantiateParticles(Vector3 pos, ParticleType type)
    {
        int i = Random.Range(0, particles[type].Count);
        InstantiateParticles(pos, type, i);
    }

    public static void InstantiateParticles(Vector3 pos, ParticleType type, int i)
    {
        InstantiateParticles(pos, new Vector3(0, 0, 0), type, i);
    }

    public static void InstantiateParticles(Vector3 pos, Vector3 rotation, ParticleType type)
    {
        int i = Random.Range(0, particles[type].Count);
        InstantiateParticles(pos, rotation, type, i);
    }

    public static void InstantiateParticles(Vector3 pos, Vector3 rotation, ParticleType type, int i)
    {
        if (i >= 0 && i < particles[type].Count)
            Destroy(Instantiate(particles[type][i], pos, Quaternion.Euler(rotation)), particles[type][i].GetComponent<ParticleSystem>().duration);

    }

    public static void InstantiateParticles(Transform pos, ParticleType type)
    {
        int i = Random.Range(0, particles[type].Count);
        {
            InstantiateParticles(pos, pos.rotation.eulerAngles, type, i);
        }
    }

    public static void InstantiateParticles(Transform pos, ParticleType type, int i)
    {
        InstantiateParticles(pos, pos.rotation.eulerAngles, type, i);
    }

    public static void InstantiateParticles(Transform pos, Vector3 lookAt, ParticleType type)
    {
        int i = Random.Range(0, particles[type].Count);
        InstantiateParticles(pos, lookAt, type, i);
    }

    public static void InstantiateParticles(Transform pos, Vector3 lookAt, ParticleType type, int i)
    {
        if (i >= 0 && i < particles[type].Count)
        {
            GameObject particle = Instantiate(particles[type][i], pos.position, Quaternion.identity) as GameObject;
            particle.transform.parent = pos;
            particle.transform.LookAt(lookAt);
            Destroy(particle, particles[type][i].GetComponent<ParticleSystem>().duration);
        }

    }
}

public enum ParticleType
{
    movementClick,
    meleeAttack,
    die,
    specialEffect,
    block,
}
