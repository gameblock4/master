﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using System;

public static class SaveAndLoad
{
    public static string path;

    static SaveAndLoad()
    {
        path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Amphyra\\savedFiles\\";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
    }

    public static T Load<T>(string fileName, Extention extention)
    {
        string file = path + fileName + GetExtention(extention);
        if (!File.Exists(file))
        {
            Debug.LogWarning("[Saving] can't find \"" + fileName + "\" at " + path);
            return default(T);
        }
        string user = new StreamReader(file).ReadToEnd();
        if (user != "")
            return JsonConvert.DeserializeObject<T>(user);
        return default(T);
    }


    public static void Save<T>(T classToSave, string fileName, Extention extention)
    {
        string file = path + fileName + GetExtention(extention);
        
        string data = JsonConvert.SerializeObject(classToSave, Formatting.Indented);
        StreamWriter u = new StreamWriter(file);
        u.Write(data);
        u.Close();
    }

    public static string GetExtention(Extention extention)
    {
        switch (extention)
        {
            case Extention.Json:
                return ".Json";
            case Extention.EERRIIKK:
                return ".EERRIIKK";
            default:
                return ".txt";
        }
    }
    
    public enum Extention
    {
        txt,
        EERRIIKK,
        Json
    }
}
