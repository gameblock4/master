﻿using UnityEngine;
using System.Collections.Generic;
using Amphyra.Items;
using System.Reflection;
using System.IO;
using Amphyra.Actors;
using Amphyra.Actors.Npcs;

public class IDManager : MonoBehaviour
{
    /// <summary>dictionary with id as key and Item as value</summary>
    private static Dictionary<int, Item> Items;
    /// <summary>dictionary with name as key and Texture2D as value</summary>
    private static Dictionary<string, Texture2D> Textures;
    /// <summary>dictionary with id as key and Npc as value</summary>
    private static Dictionary<int, Npc> NPCs;
    /// <summary>dictionary with id as key and Enemy as value</summary>
    private static Dictionary<int, Enemy> Enemies;
    /// <summary>dictionary with id as key and Actor as value</summary>
    private static Dictionary<int, Actor> Actors;

    //
    private static string path;

    /// <summary>
    /// register an item
    /// </summary>
    /// <param name="item">item to register</param>
    public static void Register(Item item)
    {
        if (Items.ContainsKey(item.ID)) { Debug.LogError("[IDManager] id: " + item.ID + "allready exists;\nItem: " + item.Name); }
        Items.Add(item.ID, item);
    }

    /// <summary>
    /// register an npc and return an id
    /// </summary>
    /// <param name="npc">npc to register</param>
    /// <returns>returns an id</returns>
    public static int Register(Npc npc)
    {
        int i = npc.ID;
        if (i == 0)
            i = NPCs.Count + 1;
        else
        if (NPCs.ContainsKey(i)) { Debug.LogError("[IDManager] id: " + i + "allready exists;\nNPC: " + npc.name); }

        NPCs.Add(i, npc);
        return i;
    }

    /// <summary>
    /// register an enemy
    /// </summary>
    /// <param name="enemy">enemy to register</param>
    public static void Register(Enemy enemy)
    {
        if (Enemies.ContainsKey(enemy.ID)) { Debug.LogError("[IDManager] id: " + enemy.ID + "allready exists;\nEnemie: " + enemy.name); }
        Enemies.Add(enemy.ID, enemy);
    }

    /// <summary>
    /// register an actor
    /// </summary>
    /// <param name="actor">actor to register</param>
    public static void Register(Actor actor)
    {
        if (Actors.ContainsKey(actor.ID)) { Debug.LogError("[IDManager] id: " + actor.ID + "allready exists;\nActor: " + actor.name); }
        Actors.Add(actor.ID, actor);
    }

    /// <summary>
    /// get the item class using the id
    /// </summary>
    /// <param name="id">id of the item</param>
    /// <returns></returns>
    public static Item GetItem(int id)
    {
        if (id == 0) return null;
        if (Items.ContainsKey(id))
            return Items[id];
        Debug.LogWarning("[IDManager] Can't find item with id: " + id);
        return null;
    }

    /// <summary>
    /// get the npc class using the id
    /// </summary>
    /// <param name="id">id of the npc</param>
    /// <returns></returns>
    public static Npc GetNpc(int id)
    {
        if (Items.ContainsKey(id))
            return NPCs[id];
        Debug.LogWarning("[IDManager] Can't find npc with id: " + id);
        return null;
    }

    /// <summary>
    /// get the enemy class using the id
    /// </summary>
    /// <param name="id">id of the enemy</param>
    /// <returns></returns>
    public static Enemy GetEnemy(int id)
    {
        if (Items.ContainsKey(id))
            return Enemies[id];
        Debug.LogWarning("[IDManager] Can't find enemy with id: " + id);
        return null;
    }

    /// <summary>
    /// get the actor class using the id
    /// </summary>
    /// <param name="id">id of the actor</param>
    /// <returns></returns>
    public static Actor GetActor(int id)
    {
        if (Items.ContainsKey(id))
            return Actors[id];
        Debug.LogWarning("[IDManager] Can't find actor with id: " + id);
        return null;
    }

    /// <summary>
    /// get the texture using the name
    /// </summary>
    /// <param name="Name">name of the texture</param>
    /// <returns></returns>
    public static Texture2D GetTexture(string Name)
    {
        if (Textures.ContainsKey(Name))
            return Textures[Name];
        Debug.LogWarning("[IDManager] Can't find Texture with name: " + Name);
        return null;
    }

    static IDManager()
    {
        NPCs = new Dictionary<int, Npc>();
        Enemies = new Dictionary<int, Enemy>();
        Actors = new Dictionary<int, Actor>();
    }

    void Awake()
    {
        string debug = "";
        path = Application.dataPath + "/Resources/";
        //print the log of loading textures and items
        debug += LoadTextures();
        debug += LoadItems();
        print(debug);
    }

    /// <summary>
    /// Load all textures
    /// </summary>
    public static string LoadTextures()
    {
        string debug = "";
        string fullPath = path + "Textures";
        if (Textures == null)
            Textures = new Dictionary<string, Texture2D>();

        //look if the folder exists. if not create it
        if (!System.IO.Directory.Exists(fullPath))
        {
            System.IO.Directory.CreateDirectory(fullPath);
            return debug;
        }

        debug += ("[IDManager] Loading Textures from: " + fullPath);

        //get all files
        string[] files = System.IO.Directory.GetFiles(fullPath);

        List<string> pngs = new List<string>();

        //check if they are a .png
        for (int i = 0; i < files.Length; i++)
        {
            if (files[i].EndsWith(".png"))
            {
                pngs.Add(files[i]);
            }
        }
        debug += ("\n[IDManager] " + pngs.Count + " Textures found");
        int loadedTextures = 0;

        for (int i = 0; i < pngs.Count; i++)
        {
            if (File.Exists(pngs[i]))
            {
                //load the image and add it to the dictionary
                byte[] fileData = File.ReadAllBytes(pngs[i]);
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);
                Textures.Add(pngs[i].Replace(fullPath + "\\", "").Replace(".png", ""), tex);
                loadedTextures++;
            }
        }
        debug += ("\n[IDManager] loaded " + loadedTextures + " textures");
        return debug;
    }

    /// <summary>
    /// Load all <see cref="Item"/> Classes
    /// </summary>
    public static string LoadItems()
    {
        string debug = "";
        string fullPath = path + "Items";
        if (Items == null)
            Items = new Dictionary<int, Item>();

        //look if the file exists. if not create it
        if (!System.IO.Directory.Exists(fullPath))
        {
            System.IO.Directory.CreateDirectory(fullPath);
            return debug;
        }

        debug += ("\n[IDManager] Loading mods from: " + fullPath);

        //get all files that are in the Items folder
        string[] files = System.IO.Directory.GetFiles(fullPath);
        debug += ("\n[IDManager] " + files.Length + " mods found");

        //this counts how many dll's finished loading
        int loadedMods = 0;
        for (int i = 0; i < files.Length; i++)
        {
            try
            {
                //if it isn't a dll continue to the next file
                if (!files[i].EndsWith(".dll")) { continue; }

                //load the assembly
                var DLL = Assembly.LoadFile(files[i]);
                debug += ("\n[IDManager] Loading " + files[i].Replace(fullPath + "\\", "") + " containing " + DLL.GetExportedTypes().Length + " classes");

                //create an instance of all classes that inherit from item from the assembly
                foreach (System.Type type in DLL.GetExportedTypes())
                {
                    if (type.IsSubclassOf(typeof(Item)))
                    {
                        System.Activator.CreateInstance(type);
                    }
                }
                debug += ("\n[IDManager] Done loading " + DLL.FullName.Split(',')[0]);
                loadedMods++;
            }
            catch (System.Exception ex)
            {
                //debug the error message
                Debug.LogError(ex.Message);
            }
        }

        debug += ("\n[IDManager] Loaded " + loadedMods + " mods");
        return debug;
    }
}
