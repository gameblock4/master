﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amphyra.Actors;
using System;
using UnityEngine.UI;
using Amphyra.Items;

public class Craftmanager : MonoBehaviour
{

    private Inventory inventory;
    [SerializeField]
    private Text warningText;
    private Dictionary<int, List<ItemStack>> craftableItems;
    public List<ItemStack>[] itemStack;

    public Craftmanager()
    {

    }

    private void Start()
    {
        inventory = Player.Main.inventory;

        craftableItems = new Dictionary<int, List<ItemStack>>();
        Debug.Log(craftableItems);
        itemStack = new List<ItemStack>[13];

        itemStack[0] = new List<ItemStack> { new ItemStack(2, 1) };
        itemStack[1] = new List<ItemStack> { new ItemStack(2, 1) };
        itemStack[2] = new List<ItemStack> { new ItemStack(2, 1) };
        itemStack[3] = new List<ItemStack> { new ItemStack(2, 1) };
        itemStack[4] = new List<ItemStack> { new ItemStack(2, 2) };
        itemStack[5] = new List<ItemStack> { new ItemStack(1, 2) };
        itemStack[6] = new List<ItemStack> { new ItemStack(3, 3) };
        itemStack[7] = new List<ItemStack> { new ItemStack(2, 3) };
        itemStack[8] = new List<ItemStack> { new ItemStack(3, 3) };
        itemStack[9] = new List<ItemStack> { new ItemStack(3, 3) };
        itemStack[10] = new List<ItemStack> { new ItemStack(3, 4) };
        itemStack[11] = new List<ItemStack> { new ItemStack(3, 4) };
        itemStack[12] = new List<ItemStack> { new ItemStack(5, 2) };

        craftableItems.Add(7, itemStack[0]);
        craftableItems.Add(8, itemStack[1]);
        craftableItems.Add(9, itemStack[2]);
        craftableItems.Add(10, itemStack[3]);
        craftableItems.Add(11, itemStack[4]);
        craftableItems.Add(12, itemStack[5]);
        craftableItems.Add(13, itemStack[6]);
        craftableItems.Add(14, itemStack[7]);
        craftableItems.Add(15, itemStack[8]);
        craftableItems.Add(16, itemStack[9]);
        craftableItems.Add(17, itemStack[10]);
        craftableItems.Add(18, itemStack[11]);
        craftableItems.Add(19, itemStack[12]);

    }

    public void CraftItem(int selectedItem)
    {


        foreach (var item in craftableItems[selectedItem])
        {
            if (!inventory.CheckForItem(item.itemID, item.amount))
            {
                warningText.text = "You dont have enough " + IDManager.GetItem(item.itemID).Name;
                return;
            }
        }

        if (!inventory.CheckIfSpaceForItem(selectedItem, 1))
        {
            warningText.text = "No space foir this item!";
            return;
        }
        inventory.AddToInventory(selectedItem, 1);
        foreach (var item in craftableItems[selectedItem])
        {
            inventory.RemoveItems(item.itemID, item.amount);
        }
        warningText.text = "crafted " + craftableItems[selectedItem];
    }

    public void Close()
    {
        gameObject.SetActive(false);
        Player.Main.isGamePaused = false;
    }
}


