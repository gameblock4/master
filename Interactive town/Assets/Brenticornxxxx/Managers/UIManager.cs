﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amphyra.Actors;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    private GameObject textUIPanel;
    [SerializeField]
    private Text panelText;
    protected string currentPanelText;
    [SerializeField]
    private Text[] npcInfoFields;
    [SerializeField]
    private Image playerLikingBar;
    [SerializeField]
    private Image[] peoplePanels;
    private int currentPage;
    [SerializeField]
    private Image[] bars;
    [SerializeField]
    private Image compas;
    [SerializeField]
    private Camera miniMapCam;
    [SerializeField]
    private RawImage worldMapContainer;
    private bool worldMapActive;
    private void Awake()
    {
        textUIPanel.SetActive(false);
        //worldMapContainer.gameObject.SetActive(false);
    }

    private void Update()
    {
        bars[0].fillAmount = (float)Player.Main.Health / (float)Player.Main.MaxHealth;
        bars[1].fillAmount = Player.Main.Stamina / 100;
        CompasRotation();
    }

    protected void PanelTextUpdate()
    {
        textUIPanel.SetActive(true);
        //   Debug.Log(textUIPanel);
        // Debug.Log(panelText);
        // Debug.Log(panelText.text);
        // Debug.Log(currentPanelText);
        panelText.text = currentPanelText;
    }

    public void TownPeopleInfo(int id)
    {
        npcInfoFields[0].text = IDManager.GetNpc(id).NpcData.name;
        npcInfoFields[1].text = "" + IDManager.GetNpc(id).NpcData.age;
        npcInfoFields[2].text = "" + IDManager.GetNpc(id).NpcData.personality;
        npcInfoFields[3].text = "" + IDManager.GetNpc(id).NpcData.job;
        npcInfoFields[4].text = "" + IDManager.GetNpc(id).NpcData.jobRole;
        npcInfoFields[5].text = "" + IDManager.GetNpc(id).NpcData.hobby;
        npcInfoFields[6].text = "" + IDManager.GetNpc(id).NpcData.favoriteSeason;

        playerLikingBar.fillAmount = IDManager.GetNpc(id).NpcData.playerLiking / 5F;
    }

    public void TownInfoPages(int pageCounter)
    {
        currentPage += pageCounter;
        if (currentPage >= peoplePanels.Length)
        {
            currentPage = 0;
        }
        else if (currentPage < 0)
        {
            currentPage = (peoplePanels.Length - 1);
        }

        for (int i = 0; i < peoplePanels.Length; i++)
        {
            if (i == currentPage)
            {
                peoplePanels[i].gameObject.SetActive(true);

            }
            else
            {
                peoplePanels[i].gameObject.SetActive(false);
            }

        }
    }

    private void CompasRotation()
    {
        Vector3 rotation = miniMapCam.transform.rotation.eulerAngles;
        compas.transform.rotation = Quaternion.Euler(new Vector3(compas.transform.rotation.eulerAngles.x, compas.transform.rotation.eulerAngles.y, rotation.y));
    }

    public void ActivateWorldMap()
    {
        worldMapActive = !worldMapActive;
        worldMapContainer.gameObject.SetActive(worldMapActive);

    }
}
