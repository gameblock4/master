﻿using UnityEngine;
using System.Collections;

public class Timer
{
  private float timeStamp;
  private float interval;
  private float pauseDifference;

  /// <summary>
  /// Sets the timer.
  /// </summary>
  /// <param name="interval">The interval.</param>
  public void SetTimer(float interval = 2)
  {
    timeStamp = Time.time;
    this.interval = interval;
  }

  public void PauseTimer(bool pause)
  {
    if(pause)
    {
      // save time (to continue after unpause)
      pauseDifference = interval - (Time.time - timeStamp);
            
      return;
    }

    // reset timestamp
    SetTimer(pauseDifference);
    // not paused 
  }

  public void StopTimer()
  {
   
  }


  public float TimerProgress()
  {
    // Time.time, interval & timeStamp
    return Mathf.Abs((timeStamp - Time.time) / interval);
    //return 0;
  }

  public bool TimerDone()
  {
    return Time.time >= timeStamp + interval
      ? true : false;

    // conditie ? true : false;
    // ? : TERNARY OPERATOR (CONDITIONAL STATEMENT)

    //if (Time.time >= timeStamp + interval)
    //{
    //  return true;
    //}

    //return false;
  }
}