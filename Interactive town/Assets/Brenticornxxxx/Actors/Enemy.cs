﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Amphyra.Actors
{
    public class Enemy : Actor
    {
        public Enemy(int healt, int id) : base(Behavior.Aggressive, healt, id) { }

        /// <summary>
        /// closest actor, this is updated about every 1 second
        /// </summary>
        protected Actor bestActorToAttack;

        /// <summary>makes sure enemies don't register</summary>
        protected override void Register() { }

        /// <summary>gets the closest actor to attack every 0,5 to 1,5 seconds</summary>
        protected IEnumerator GetObjectsToAttack()
        {
            //while the gameobjects is not null
            while (gameObject != null)
            {
                List<Actor> actors = new List<Actor>();

                //get all colliders within a range of 10
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10);

                //check if the collider has an actor script and if it is not aggressive
                for (int i = 0; i < hitColliders.Length; i++)
                {
                    Actor actor = hitColliders[i].GetComponent<Actor>();
                    if (actor)
                    {
                        if (actor.Behavior != Behavior.Aggressive)
                        {
                            actors.Add(actor);
                        }
                    }
                }

                float distace = float.MaxValue;

                bestActorToAttack = null;
                //get the closest actor
                for (int i = 0; i < actors.Count; i++)
                {
                    float dis = Vector3.Magnitude(transform.position - actors[i].transform.position);
                    if (dis < distace)
                    {
                        bestActorToAttack = actors[i];
                        distace = dis;
                    }
                }
                //wait
                yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            }
        }
    }
}