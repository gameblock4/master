﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Amphyra.Actors
{
    public class ActorGroup : MonoBehaviour
    {

        [SerializeField]
        private bool isRecalculating = true;
        /// <summary>actors in this group</summary>
        public List<Actor> actors;

        /// <summary>actor to instantiate</summary>
        [SerializeField]
        private GameObject objectWithActor;

        /// <summary>the recalculate coroutine</summary>
        private Coroutine recalculate;

        /// <summary>the minimum amount of objects to spawn</summary>
        [SerializeField]
        private int minimumSpawnedActors;

        /// <summary>the maximum amount of objects to spawn</summary>
        [SerializeField]
        private int maximumSpawnedActors;

        [Range(0, 200)]
        public int distanceForSpawn;


        public void Start()
        {
            //move to a position 1 above the gound. if there is no ground destroy this
            //Ray ray = new Ray(transform.position, Vector3.down);
            //RaycastHit hit;
            //if (Physics.Raycast(ray, out hit, 150))
            //{
            //    transform.position = hit.point + Vector3.up;
            //}
            //else
            //{
            //    Destroy(this);
            //    return;
            //}
            //instantiate the objects and register them
            for (int i = 0; i < Random.Range(minimumSpawnedActors, maximumSpawnedActors); i++)
            {
                actors.Add((Instantiate(objectWithActor, transform.position + new Vector3(Random.Range(-10, 10), transform.position.y, Random.Range(-10, 10)), Quaternion.identity) as GameObject).GetComponent<Actor>().RegisterGroup(this));
                actors[i].transform.parent = transform.parent;
            }
            recalculate = StartCoroutine(Recalculate());
        }

        /// <summary>recalculate the middle point every 10 second unil there are no actors anymore. this also spawns new actors when the player is out of range</summary>
        private IEnumerator Recalculate()
        {
            while (isRecalculating)
            {
                //recalculate the middle
                Vector3 newMiddle = new Vector3(0, 0, 0);
                for (int i = 0; i < actors.Count; i++)
                {
                    newMiddle += actors[i].transform.position;
                }
                //set the middle
                if (newMiddle != Vector3.zero)
                    transform.position = newMiddle / actors.Count;

                //spawn new actor if there are less actors than the avarage of minimumSpawnedActors and maximumSpawnedActors and when the player is out of range
                if (actors.Count < (minimumSpawnedActors + maximumSpawnedActors) / 2 && (transform.position - Player.Main.transform.position).sqrMagnitude > distanceForSpawn * distanceForSpawn)
                {
                    actors.Add((Instantiate(objectWithActor, transform.position + new Vector3(Random.Range(-10, 10), transform.position.y, Random.Range(-10, 10)), Quaternion.identity) as GameObject).GetComponent<Actor>().RegisterGroup(this));
                    actors[actors.Count - 1].transform.parent = transform.parent;
                }
                yield return new WaitForSeconds(10);
            }
        }
    }
}