﻿using UnityEngine;
using Amphyra.Town;

namespace Amphyra.Actors.Npcs
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Rigidbody))]
    public class Npc : Actor
    {
        /// <summary>State machine for the npcs AI</summary>
        public NpcStateMachine stateMachine;

        /// <summary>NPC info</summary>
        [SerializeField]
        protected NpcStruct npcData;

        //the MeshRenderer and Collider of this npc
        private MeshRenderer _renderer;
        private Collider _collider;

        /// <summary>the NavMeshAgent of this object</summary>
        public NavMeshAgent pathFinding;

        /// <summary>is the npc in a building</summary>
        protected bool isInBuilding;
        /// <summary>the building the npc is in</summary>
        protected Building currentBuilding;

        /// <summary>is the npc in a building</summary>
        public bool IsInBuilding
        {
            get { return isInBuilding; }
        }

        /// <summary>the building the npc is in</summary>
        public Building CurrentBuilding
        {
            get { return currentBuilding; }
        }

        /// <summary>NPC info</summary>
        public NpcStruct NpcData
        {
            get { return npcData; }
        }

        public Npc(int maxHealth, int id) : base(Behavior.Passive, maxHealth, id)
        {
        }

        public Npc(int maxHealth, int id, int inventorySize, int unlockedInventorySlots) : base(maxHealth, id, Behavior.Passive, inventorySize, unlockedInventorySlots)
        {
        }

        /// <summary>register with the IDManager</summary>
        protected override void Register()
        {
            id = IDManager.Register(this);
        }

        public void Start()
        {
            //get the navMeshAgent
            pathFinding = GetComponent<NavMeshAgent>();
            //set the move and rotate speed 
            pathFinding.speed = speed;
            pathFinding.angularSpeed = rotateSpeed * (360 / 5f);
            //create the statemachine
            stateMachine = new NpcStateMachine(this, 10);

            //set the food level to a random number
            npcData.FoodLevel = Random.Range(5, NpcStruct.maxFood);

            //start updating the current statemachine
            StartCoroutine(stateMachine.UpdateState());

            //get the collider and meshrenderer
            _collider = GetComponent<Collider>();
            _renderer = GetComponent<MeshRenderer>();
        }

        /// <summary>
        /// this method is disabled
        /// </summary>
        /// <param name="moveTarget"></param>
        /// <param name="rotateTarget"></param>
        /// <param name="range"></param>
        /// <param name="deltaSpeed"></param>
        protected override void Move(Vector3 moveTarget, Quaternion rotateTarget, float range, float deltaSpeed) { }

        public void OnCollisionEnter(Collision col)
        {
            Building building = col.gameObject.GetComponent<Building>();
            if (building)
            {
                EnterBuilding(building);
            }
        }

        /// <summary>
        /// enter a building
        /// </summary>
        /// <param name="building">the building to enter</param>
        public void EnterBuilding(Building building)
        {
            EnterOrExitBuilding(true, building);
        }

        /// <summary>
        /// leave the current building
        /// </summary>
        public void LeaveBuilding()
        {
            EnterOrExitBuilding(false, currentBuilding);
        }

        /// <summary>
        /// enter or exit a building
        /// </summary>
        /// <param name="enter">should the npc enter</param>
        /// <param name="building">building to enter(leaving will always use the current building)</param>
        public void EnterOrExitBuilding(bool enter, Building building)
        {
            //if the npc should enter
            if (enter)
            {
                //disable the renderer and the collider
                _renderer.enabled = false;
                _collider.enabled = false;
                //set the currentBuilding
                currentBuilding = building;
                //set the posision and stop moving
                transform.position = currentBuilding.transform.position + currentBuilding.buildingExit;
                pathFinding.Stop();
                //enter the currentbuilding and set isInBuilding true
                currentBuilding.EnterBuilding(this);
                isInBuilding = true;
                //call the OnBuildingEnter method of the stateMachine
                stateMachine.currentstate.OnBuildingEnter(currentBuilding);
            }
            else
            {
                //tell the current building you left
                currentBuilding.ExitBuilding(id);
                //set the posision to the exit of the building
                transform.position = currentBuilding.transform.position + currentBuilding.buildingExit;
                //enable the renderer and the collider
                _renderer.enabled = true;
                _collider.enabled = true;
                //call the ExitBuilding method of the stateMachine
                stateMachine.currentstate.OnBuildingExit(currentBuilding);
                //set isInBuilding false(I know you can see that)
                isInBuilding = false;
            }
        }
    }
}
