﻿namespace Amphyra.Actors.Npcs
{

    /// <summary>
    /// what kind of personality does this npc have
    /// </summary>
    public enum PersonalityEnum
    {
        Social,
        AntiSocial,
        Hasty,
        Lazy,
        Grumpy,
        Caring,
    }
}