﻿using UnityEngine;

namespace Amphyra.Actors.Npcs
{
    public class NpcSpeech : UIManager
    {

        public string[] morningText;
        public string[] dayText;
        public string[] eveningText;
        public static GameObject selectedObject;

        //Must go when daynight cycle script excists(needs to be static)
        public DayTimeEnum currentDayPart;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                InteractHitNPC();
            }
        }


        public void InteractHitNPC()
        {
            // if (!importantNPC)
            // {
            if (currentDayPart == DayTimeEnum.Morning)
            {
                currentPanelText = morningText[Random.Range(0, morningText.Length)];
                //use morningText array
            }
            else if (currentDayPart == DayTimeEnum.Day)
            {
                currentPanelText = dayText[Random.Range(0, dayText.Length)];
                //use dayText array
            }
            else if (currentDayPart == DayTimeEnum.Evening)
            {
                currentPanelText = eveningText[Random.Range(0, eveningText.Length)];
                //use eveningText array
            }

            PanelTextUpdate();

            // }

            //else if(importantNPC)
            //{


            //}
        }
    }
}