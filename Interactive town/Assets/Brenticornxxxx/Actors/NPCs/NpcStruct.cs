﻿namespace Amphyra.Actors.Npcs
{

    /// <summary>
    /// all info about the npc
    /// </summary>
    [System.Serializable]
    public class NpcStruct
    {
        [UnityEngine.SerializeField]
        protected int foodLevel; //0 t/m 25
        public string name;
        public int age;
        public JobEnum job;
        public JobRoleEnum jobRole;
        public float playerLiking;
        public int familyID;
        public int favoriteGift;
        public int[] friends;
        public SeasonEnum favoriteSeason;
        public ActivatyEnum hobby;
        public ActivatyEnum currentActivaty;
        public bool importantNPC;
        public PersonalityEnum personality;

        public const int maxFood = 25;

        public int FoodLevel
        {
            get { return foodLevel; }
            set
            {
                foodLevel = value;
                if (foodLevel > maxFood) foodLevel = maxFood;
                if (foodLevel < 0) foodLevel = 0;
            }
        }
    }
}