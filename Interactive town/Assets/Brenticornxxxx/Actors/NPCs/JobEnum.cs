﻿namespace Amphyra.Actors.Npcs
{
    /// <summary>
    /// type of Job for the NPC
    /// </summary>

    public enum JobEnum
    {
        Blacksmith,
        Mayor,
        Farmer,
        Fisher,
        Merchant,
        Guard,
        InnHost,
        Pirate,
        Traveler,
        UnEmployed,
    }
}