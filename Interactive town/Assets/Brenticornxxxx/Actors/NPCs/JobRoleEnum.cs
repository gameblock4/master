﻿namespace Amphyra.Actors.Npcs
{

    /// <summary>
    /// the NPC's role with the job
    /// </summary>
    public enum JobRoleEnum
    {
        Assistant,
        Owner,
        Worker,
        None,
    }
}