﻿namespace Amphyra.Actors.Npcs
{

    /// <summary>
    /// current activaty of the actor
    /// </summary>
    public enum ActivatyEnum
    {
        Working,
        Sleeping,
        Wandering,
        Socializing,
        Playing,
    }
}