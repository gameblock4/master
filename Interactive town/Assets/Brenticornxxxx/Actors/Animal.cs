﻿using UnityEngine;
using System.Collections;

namespace Amphyra.Actors.Animals
{
    public class Animal : Actor
    {
        ///<summary>target to move towards</summary>
        protected Vector3 walkTarget;

        public Animal(int healt, int dropItem = 12, int dropAmount = 2) : base(Behavior.Passive, healt, dropItem, dropAmount) { }

        //makes sure that the animal needs to have its own way of registering
        protected override void Register() { }

        /// <summary>
        /// start the moving coroutine
        /// </summary>
        void Start()
        {
            StartCoroutine(Moving());
        }

        /// <summary>
        /// move
        /// </summary>
        void Update()
        {
            //move towards the walktarget
            Move(walkTarget, Quaternion.LookRotation(transform.position - walkTarget), 4, Time.deltaTime);
        }

        /// <summary>move towards the group unless this got attacked</summary>
        protected virtual IEnumerator Moving()
        {
            while (group != null)
            {
                //if this got hit wait 10 seconds and return to the normal behavior
                if (isMoving)
                {
                    yield return new WaitForSeconds(10);
                    isMoving = false;
                }
                else
                {
                    //set te walktarget to a random target and move towards it for 3 seconds
                    walkTarget = new Vector3(Random.Range(-50, 50), 0, Random.Range(-50, 50));
                    if (group != null)
                    {
                        walkTarget += new Vector3(group.transform.position.x, 0, group.transform.position.z);
                    }
                    yield return new WaitForSeconds(3);
                }
            }
        }

        /// <summary>when hit move away from the attacker</summary>
        public override bool Hit(int damage, Actor attacker, AttackType attackType)
        {
            isMoving = true;
            walkTarget = (transform.position - attacker.transform.position).normalized * 40 + transform.position;
            return base.Hit(damage, attacker, attackType);
        }
    }
}
