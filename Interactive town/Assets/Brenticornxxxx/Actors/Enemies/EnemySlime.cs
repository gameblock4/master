﻿using UnityEngine;

namespace Amphyra.Actors
{
    public class EnemySlime : Enemy
    {
        //where to go when you dont have something to attack
        private Vector3 posisionFromMiddle;

        public EnemySlime() : base(5, 2)
        {
        }

        public override void Awake()
        {
            base.Awake();
            //start a loop to get all near actors to attack and get the closest one
            StartCoroutine(GetObjectsToAttack());

            //set it to a  random posision
            posisionFromMiddle = new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
        }

        void Update()
        {
            //if it has something to attack move towards it and if it is not attacking something start attacking it
            if (bestActorToAttack)
            {
                if (attackTarget)
                    Move(attackTarget.transform.position, Quaternion.identity, 2, Time.deltaTime);
                if (!isAttacking)
                    StartAttacking(bestActorToAttack);
            }
            //if it doesn't have something to movetowards move back to the posision from middle
            else if (!isMoving)
            {

                Move(group.transform.position + posisionFromMiddle, Quaternion.LookRotation(transform.position - group.transform.position), 2, Time.deltaTime);

                if (((group.transform.position + posisionFromMiddle) - transform.position).magnitude < 3)
                {
                    //if it is at his destenation get a new one
                    posisionFromMiddle = new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
                }
            }
        }
    }
}