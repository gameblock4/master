﻿using UnityEngine;

/// <summary>
/// base class for everything that the player can left click
/// </summary>
public abstract class Interacteble : MonoBehaviour
{
    public abstract void Interact();
}
