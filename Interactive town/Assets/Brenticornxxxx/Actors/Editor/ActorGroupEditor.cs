﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Amphyra.Actors;

[CustomEditor(typeof(ActorGroup))]
public class ActorGroupEditor : Editor
{
    ActorGroup group;
    void OnEnable()
    {
        group = ((ActorGroup)target);
    }

    void OnSceneGUI()
    {
        float exitPosision = Handles.ScaleSlider(group.distanceForSpawn, group.transform.position, Vector3.right, group.transform.rotation, 50, 1);
        group.distanceForSpawn = (int)exitPosision;
        Handles.color = new Color(0, 1, 0, 0.5f);
        Handles.SphereCap(0, group.transform.position, group.transform.rotation, group.distanceForSpawn * 2);

    }
}
