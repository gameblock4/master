﻿using UnityEngine;
using System.Collections;
using Amphyra.Items;

namespace Amphyra.Actors
{
    [RequireComponent(typeof(Rigidbody))]
    public class Actor : Interacteble
    {
        /// <summary>group of this actor. this is null when this doesn't have an actor</summary>
        protected ActorGroup group;

        /// <summary>the rigidbody of the actor</summary>
        protected Rigidbody _rigidbody;

        /// <summary>health of the actor</summary>
        [SerializeField]
        protected int health;
        /// <summary>Maximum health of the actor(also the startHealth)</summary>
        protected int maxHealth;

        /// <summary>actors id</summary>
        protected int id;

        /// <summary>actors id</summary>
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// inventory of the actor
        /// </summary>
        public Inventory inventory;


        /// <summary>
        /// is this actor attacking
        /// </summary>
        protected bool isAttacking = false;

        /// <summary>set this to true to stop attacking</summary>
        protected bool stopAttacking;

        /// <summary>the target to attack</summary>
        [SerializeField]
        protected Actor attackTarget;

        /// <summary>the attacking coroutine. </summary>
        protected Coroutine attacking;

        /// <summary>is the actor moving</summary>
        protected bool isMoving;

        /// <summary>the speed the player rotates with in</summary>
        [SerializeField]
        [Range(1, 5)]
        protected float rotateSpeed;

        /// <summary>the rotation from the player toward the moveTarget</summary>
        protected Quaternion targetRotation;

        /// <summary>
        /// armor that the actor is using
        /// </summary>
        [SerializeField]
        protected ArmorInventory armor;

        /// <summary>
        /// armor that the actor is using
        /// </summary>
        public ArmorInventory Armor
        {
            get { return armor; }
        }

        /// <summary>the item that gets dropped when killed</summary>
        [SerializeField]
        protected int dropItem;
        /// <summary>the amount of loot you get when killed</summary>
        [SerializeField]
        protected int dropAmount;

        /// <summary>the speed the player moves in</summary>
        [SerializeField]
        [Range(0, 20)]
        protected float speed;

        /// <summary>the behavior of this actor</summary>
        protected Behavior behavior;
        /// <summary>the behavior of this actor</summary>
        public Behavior Behavior
        {
            get { return behavior; }
        }

        /// <summary>health of the actor</summary>
        public int Health
        {
            get { return health; }
        }

        /// <summary>Maximum health of the actor(also the startHealth)</summary>
        public int MaxHealth
        {
            get { return maxHealth; }
        }

        protected float fallSpeed;

        /// <summary>
        /// Actors constructor
        /// </summary>
        /// <param name="maxHealth">max health and start health</param>
        /// <param name="id">id of the actor</param>
        /// <param name="behavior">behavior for the actor</param>
        /// <param name="inventorySize">Size of the inventory</param>
        /// <param name="unlockedInventorySlots">how many slots are unlocked from the beginning</param>
        /// <param name="dropItem">when this actor is killed what does the attacker get</param>
        /// <param name="dropAmount">how many drop items does the attacker get</param>
        public Actor(int maxHealth, int id, Behavior behavior, int inventorySize, int unlockedInventorySlots, int dropItem = 12, int dropAmount = 2)
        {
            this.maxHealth = maxHealth;
            health = maxHealth;
            inventory = new Inventory(inventorySize, unlockedInventorySlots);
            this.behavior = behavior;
            this.dropItem = dropItem;
            this.dropAmount = dropAmount;
            this.id = id;
            armor = new ArmorInventory(0, 0, 0, 0, 0);
        }

        /// <summary>
        /// Actors constructor
        /// </summary>
        /// <param name="behavior">behavior for the actor</param>
        /// <param name="maxHealth">max health and start health</param>
        /// <param name="id">id of the actor</param>
        /// <param name="dropItem">when this actor is killed what does the attacker get</param>
        /// <param name="dropAmount">how many drop items does the attacker get</param>
        public Actor(Behavior behavior, int maxHealth, int id, int dropItem = 12, int dropAmount = 2)
        {
            this.maxHealth = maxHealth;
            health = maxHealth;
            this.behavior = behavior;
            this.dropItem = dropItem;
            this.dropAmount = dropAmount;
            this.id = id;
            armor = new ArmorInventory(0, 0, 0, 0, 0);
        }

        public virtual void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            //register this actor
            Register();
        }

        /// <summary>register at the IDManager</summary>
        protected virtual void Register()
        {
            IDManager.Register(this);
        }

        /// <summary>
        /// register at the actor
        /// </summary>
        /// <param name="group">the group to register</param>
        /// <returns>returns the actor</returns>
        public Actor RegisterGroup(ActorGroup group)
        {
            this.group = group;
            return this;
        }

        /// <summary>
        /// will damage this actor and return true if this dies
        /// </summary>
        /// <param name="damage">the damage delt</param>
        /// <param name="attacker">who attacked</param>
        /// <param name="attackType">type of weapon used</param>
        /// <returns>returs true if the actor died</returns>
        public virtual bool Hit(int damage, Actor attacker, AttackType attackType)
        {
            
            //if you are not attacking and you arn't passive start attacking
            if ((!isAttacking || attackTarget == null) && behavior != Behavior.Passive)
            { 
                //if your old target is to far away start attacking the attacker
                if (attackTarget == null)
                    StartAttacking(attacker);
                else if ((transform.position - attackTarget.transform.position).sqrMagnitude > 16)
                    StartAttacking(attacker);
            }

            //look how much damage is blocked using the shield
            int damageBlocked = 0;
            Item item = armor.GetShield();
            if (item != null)
            {
                if (item.Type == ItemType.ShieldItem)
                {
                    if (((ShieldItem)item).IsProtectingFrom(attackType))
                    {
                        int blockedPer = ((ShieldItem)item).ReductionPercentage;
                        damageBlocked = Mathf.RoundToInt((damage / 100f) * blockedPer);
                        ((ShieldItem)item).OnBlock(attacker, this);
                    }
                }
            }
            //remove the health
            health -= (damage - damageBlocked);

            //when there is no health left die and return true
            //if the attacker whas the player add the dropitem to his inventory
            if (health <= 0)
            {
                Die();
                if (attacker == Player.Main)
                {
                    Player.Main.CollectItem(dropItem, dropAmount);
                }
                //instantiate die particles
                ParticleManager.InstantiateParticles(transform.position, Vector3.up, ParticleType.die);
                return true;
            }
            //instantiate meleeAttack particles
            else ParticleManager.InstantiateParticles(transform, transform.up, ParticleType.meleeAttack);
            return false;
        }

        /// <summary>this is called when you die</summary>
        public virtual void Die()
        {
            if (group != null)
                group.actors.Remove(this);
            Destroy(gameObject);
        }

        /// <summary>what to do when the player left clicks this</summary>
        public override void Interact()
        {
            Debug.LogWarning('[' + name + "] doesn't override \"void Interact\"");
        }

        /// <summary>
        /// move and rotate the player towards the targets
        /// </summary>
        /// <param name="moveTarget">target to move towards</param>
        /// <param name="rotateTarget">target to rotate towards</param>
        /// <param name="range">how close to go towards the moveTarget</param>
        /// <param name="deltaSpeed">time between updates(Time.deltaTime or Time.fixedDeltaTime)</param>
        protected virtual void Move(Vector3 moveTarget, Quaternion rotateTarget, float range, float deltaSpeed)
        {
            //if you fall to low you die
            if (transform.position.y < -100)
            {
                Die();
                return;
            }

            //rotate the actor closer to the rotateTarget
            transform.rotation = (Quaternion.Lerp(transform.rotation, rotateTarget, deltaSpeed * rotateSpeed));

            //set the x and z rotations to 0 to prevent spastic movement
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

            //target speed and direction
            Vector3 target = ((moveTarget - transform.position).normalized) * speed;

            //prevent the actor from moving in the air
            target.y = _rigidbody.velocity.y;

            //if the actor is not within range set the velocity else stop moving
            if ((moveTarget - transform.position).sqrMagnitude > range * range + 0.1f)
            {
                _rigidbody.velocity = target;
            }
            else
            {
                isMoving = false;
            }
        }

        private void FixedUpdate()
        {

            fallSpeed = _rigidbody.velocity.y;
        }

        /// <summary>
        /// start moving towards and attacking the actor
        /// </summary>
        /// <param name="enemy">what to attack</param>
        protected virtual void StartAttacking(Actor enemy)
        {
            StartAttacking(enemy, 4);
        }

        /// <summary>
        /// start moving towards and attacking the actor
        /// </summary>
        /// <param name="enemy">what to attack</param>
        /// <param name="range">how close does this actor have to get</param>
        protected virtual void StartAttacking(Actor enemy, float range)
        {
            //if it was allready dont start attacking
            if (enemy == attackTarget)
            {
                return;
            }
            if (attacking != null) StopCoroutine(attacking);
            attacking = StartCoroutine(Attack(enemy, range));
        }

        /// <summary>
        /// stopA atacking
        /// </summary>
        protected virtual void StopAttacking()
        {
            //i wasn't sure if my old method was working
            StopCoroutine(attacking);
            isAttacking = false;
            attackTarget = null;
        }

        /// <summary>
        /// this will keep attacking the enemy until its dead or out of range
        /// </summary>
        /// <param name="enemy">who to attack</param>
        /// <param name="range">how close to get</param>
        /// <returns></returns>
        protected virtual IEnumerator Attack(Actor enemy, float range)
        {
            //set is attacking to true and store the enemy in this class
            isAttacking = true;
            attackTarget = enemy;

            //set it to the max value so it will allways go true it once
            float sqrDistance = int.MaxValue;

            //wait one second
            yield return new WaitForSeconds(1);
            //check if the enemy is null if so stop attacking(the enemy is probebly killed by someone else)
            if (enemy == null)
            {
                StopAttacking();
                yield break;
            }
            //while not in range wait and repeat unil in range
            while (sqrDistance > range * range)
            {
                yield return new WaitForSeconds(0.1f);
                if (enemy == null)
                {
                    StopAttacking();
                    yield break;
                }
                if ((transform.position - enemy.transform.position).sqrMagnitude > range * range * 1.3f)
                {
                    StopAttacking();
                    yield break;
                }
                sqrDistance = (transform.position - enemy.transform.position).sqrMagnitude;
            }
            //while the enemie has health
            while (enemy.Health > 0)
            {
                //check if the enemy is null if so stop attacking(the enemy is probebly killed by someone else)
                if (enemy == null)
                {
                    StopAttacking();
                    yield break;
                }
                //if out of range stop attacking
                if ((transform.position - enemy.transform.position).sqrMagnitude > range * range)
                {
                    StopAttacking();
                    yield break;
                }
                //set standard delay to 3 ,damage to 1 and the type to slashing
                float delay = 3;
                int damage = 1;
                AttackType type = AttackType.Slashing;

                //get the item class of the hand item
                Item item = armor.GetHandItem();

                //if the item is not null set the damage, attacktype and delay to the items damage, attacktype and delay
                if (item != null)
                {
                    if (item.Type == ItemType.AttackItem)
                    {
                        damage = ((AttackItem)item).Damage;
                        type = ((AttackItem)item).AttackType;
                        delay = ((AttackItem)item).AttackDelay;
                    }
                }
                //else if the item is not null debug a warning that the item with that id couldn't be found
                else if (armor.handItem != 0)
                {
                    print("[" + name + "] cant find item with id: " + armor.handItem);
                    StopAttacking();
                    yield break;
                }
                //damage the enemy
                enemy.Hit(damage, this, type);

                //tell the item it is attacking(he is a verry nice guy)
                if (item != null)
                    if (item.Type == ItemType.AttackItem)
                        ((AttackItem)item).OnAttack(enemy, this);

                //wait for the delay
                yield return new WaitForSeconds(delay);
            }
            //stop attacking(cycle completed)
            StopAttacking();
            yield break;
        }
    }
}

/// <summary>
/// behavior types
/// </summary>
public enum Behavior
{
    Passive,
    Aggressive,
    Neutral,
}