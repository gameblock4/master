﻿using System.Collections.Generic;
using UnityEngine;

namespace QRCoder
{
    public class UnityQRCode : AbstractQRCode<Texture2D>
    {
        public UnityQRCode(QRCodeData data) : base(data) { }

        public override Texture2D GetGraphic(int pixelsPerModule)
        {
            return this.GetGraphic(pixelsPerModule, Color.black, Color.white);
        }

        public Texture2D GetGraphic(int pixelsPerModule, string darkColorHtmlHex, string lightColorHtmlHex)
        {
            return this.GetGraphic(pixelsPerModule, HexToColor(darkColorHtmlHex), HexToColor(lightColorHtmlHex));
        }

        public static Color HexToColor(string hexColor)
        {
            hexColor = hexColor.Replace("0x", "").Replace("#", "").Trim();
            byte a = 255;
            var r = byte.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            var g = byte.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            var b = byte.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            if (hexColor.Length == 8)
            {
                a = byte.Parse(hexColor.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }

        public Texture2D GetGraphic(int pixelsPerModule, Color darkColor, Color lightColor)
        {
            var size = this.QrCodeData.ModuleMatrix.Count * pixelsPerModule;
            var gfx = new Texture2D(size, size, TextureFormat.ARGB32, false);
            var darkBrush = this.GetBrush(pixelsPerModule, pixelsPerModule, darkColor);
            var lightBrush = this.GetBrush(pixelsPerModule, pixelsPerModule, lightColor);
            for (var x = 0; x < size; x = x + pixelsPerModule)
            {
                for (var y = 0; y < size; y = y + pixelsPerModule)
                {
                    var module = this.QrCodeData.ModuleMatrix[(y + pixelsPerModule) / pixelsPerModule - 1][(x + pixelsPerModule) / pixelsPerModule - 1];
                    if (module)
                        gfx.SetPixels(x, y, pixelsPerModule, pixelsPerModule, darkBrush);
                    else
                        gfx.SetPixels(x, y, pixelsPerModule, pixelsPerModule, lightBrush);
                }
            }

            gfx.Apply();
            return gfx;
        }

        public Texture2D GetGraphic(int pixelsPerModule, Color darkColor, Color lightColor, Texture2D icon, int iconSizePercent = 15, int iconBorderWidth = 6)
        {
            var size = this.QrCodeData.ModuleMatrix.Count * pixelsPerModule;
            var gfx = new Texture2D(size, size, TextureFormat.ARGB32, false);
            var darkBrush = this.GetBrush(pixelsPerModule, pixelsPerModule, darkColor);
            var lightBrush = this.GetBrush(pixelsPerModule, pixelsPerModule, lightColor);
            bool drawIconFlag = icon != null && iconSizePercent > 0 && iconSizePercent <= 100;

            float iconDestWidth = 0, iconDestHeight = 0, iconX = 0, iconY = 0;

            if (drawIconFlag)
            {
                iconDestWidth = iconSizePercent * gfx.width / 100f;
                iconDestHeight = drawIconFlag ? iconDestWidth * icon.height / icon.width : 0;
                iconX = (gfx.width - iconDestWidth) / 2;
                iconY = (gfx.height - iconDestHeight) / 2;
            }

            for (var x = 0; x < size; x = x + pixelsPerModule)
            {
                for (var y = 0; y < size; y = y + pixelsPerModule)
                {
                    var module = this.QrCodeData.ModuleMatrix[(y + pixelsPerModule) / pixelsPerModule - 1][(x + pixelsPerModule) / pixelsPerModule - 1];
                    if (module)
                    {
                        gfx.SetPixels(x, y, pixelsPerModule, pixelsPerModule, darkBrush);
                    }
                    else
                        gfx.SetPixels(x, y, pixelsPerModule, pixelsPerModule, lightBrush);
                }
            }

            for (int x = (int)iconX; x < iconDestWidth + iconX; x++)
            {
                for (int y = (int)iconY; y < iconDestHeight + iconY; y++)
                {
                    gfx.SetPixel(x, y, icon.GetPixel(x - (int)iconX, y - (int)iconY));
                }
            }

            gfx.Apply();
            return gfx;
        }

        internal Color[] GetBrush(int sizeX, int sizeY, Color defaultColor)
        {
            var len = sizeX * sizeY;
            var brush = new List<Color>(len);
            for (var i = 0; i < len; i++)
            {
                brush.Add(defaultColor);
            }

            return brush.ToArray();
        }
    }
}