﻿using UnityEngine;
using System.Collections;
using QRCoder;
using UnityEngine.UI;
using Amphyra.Actors;
using System.IO;

public static class QR
{
    public static string[] codes;
    public static int seed;

    public static QRCodeGenerator QRGenerator;

    public static Sprite[] QRCodes;

    static QR()
    {
        codes = new string[3];
        QRCodes = new Sprite[codes.Length];
        QRGenerator = new QRCodeGenerator();
    }

    public static void GenerateQRCode()
    {
        int seed = SaveAndLoad.Load<int>("randomSeed", SaveAndLoad.Extention.txt);
        if (seed == 0)
        {
            seed = Random.Range(0, int.MaxValue);
            SaveAndLoad.Save(seed, "randomSeed", SaveAndLoad.Extention.txt);
        }
        System.Random ran = new System.Random(seed);
        for (int i = 0; i < codes.Length; i++)
        {
            codes[i] = "";
            for (int letter = 0; letter < 6; letter++)
            {
                codes[i] += ToString(ran.Next(0, letters.Length));
            }
            Texture2D text = new UnityQRCode((QRGenerator.CreateQrCode(codes[i], QRCodeGenerator.ECCLevel.H))).GetGraphic(20, Color.black, Color.white, Player.Main.QRImage, 22, 3);
            Rect rect = new Rect(0, 0, text.width, text.height);
            QRCodes[i] = Sprite.Create(text, rect, new Vector2(0.5f, 0.5f));
        }
    }

    public static Texture2D FillRactangle(Texture2D text, Color col, Rect rect)
    {
        for (int x = (int)rect.x; x < rect.width + rect.x; x++)
        {
            for (int y = (int)rect.y; y < rect.height + rect.y; y++)
            {
                text.SetPixel(x, y, col);
            }
        }
        return text;
    }

    private static char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvw1234567890".ToCharArray();

    public static char ToString(int i)
    {
        return letters[Mathf.Clamp(i, 0, letters.Length)];
    }

    public static bool ControlQRCode(string code)
    {
        for (int i = 0; i < codes.Length; i++)
        {
            if (code == codes[i]) return true;
        }
        return false;
    }

    public static int GetQRcodeIndex(string code)
    {
        if (ControlQRCode(code))
        {
            for (int i = 0; i < codes.Length; i++)
            {
                if (codes[i] == code)
                    return i;
            }
        }
        return -1;
    }

    public static Sprite ToSprite(this Texture2D text)
    {
        Rect rect = new Rect(0, 0, text.width, text.height);
        return Sprite.Create(text, rect, new Vector2(0.5f, 0.5f));
    }
}
