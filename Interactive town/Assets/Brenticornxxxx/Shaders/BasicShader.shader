﻿Shader "Amphyra/Basic" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
	_MainCol("Main Color", Color) = (1,1,1,1)
	}

	SubShader{

	Tags{ "RenderType" = "Opaque" }

	CGPROGRAM

	#pragma surface surf Lambert

	struct Input {
		float2 uv_MainTex;
	};
	sampler2D _MainTex;
	float4 _MainCol;
	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * _MainCol;
	}
	ENDCG
	}
		Fallback "Diffuse"
}