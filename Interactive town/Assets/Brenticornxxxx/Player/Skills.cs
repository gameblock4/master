﻿using UnityEngine;
using System.Collections;
using Amphyra.Actors;

[System.Serializable]
public class Skills
{
    public int[] expNeeded;

    public Skills()
    {
        expNeeded = new int[]
            {
                5,10,15,25,35,50, 65, 75,90,int.MaxValue
            };
    }

    /// <summary>Fishing level.<para>Starts at 0 and the max is 10.</para></summary>
    public int fishing;
    /// <summary>Fishing exp left to get to the next level</summary>
    public int fishingExpLeft;
    /// <summary>Mining level.<para>Starts at 0 and the max is 10.</para></summary>
    public int mining;
    /// <summary>Mining exp left to get to the next level</summary>
    public int miningExpLeft;
    /// <summary>Cooking level.<para>Starts at 0 and the max is 10.</para></summary>
    public int cooking;
    /// <summary>Cooking exp left to get to the next level</summary>
    public int cookingExpLeft;
    /// <summary>Farming level.<para>Starts at 0 and the max is 10.</para></summary>
    public int farming;
    /// <summary>Farming exp left to get to the next level</summary>
    public int farmingExpLeft;
    /// <summary>Trading level.<para>Starts at 0 and the max is 10.</para></summary>
    public int trading;
    /// <summary>Trading exp left to get to the next level</summary>
    public int tradingExpLeft;
    /// <summary>Attacking level.<para>Starts at 0 and the max is 10.</para></summary>
    public int attacking;
    /// <summary>Attacking exp left to get to the next level</summary>
    public int attackingExpLeft;
    /// <summary>Defence level.<para>Starts at 0 and the max is 10.</para></summary>
    public int defence;
    /// <summary>Defence exp left to get to the next level</summary>
    public int defenceExpLeft;
    /// <summary>Crafting level.<para>Starts at 0 and the max is 10.</para></summary>
    public int crafting;
    /// <summary>Crafting exp left to get to the next level</summary>
    public int craftingExpLeft;
    /// <summary>Gathering level.<para>Starts at 0 and the max is 10.</para></summary>
    public int gathering;
    /// <summary>Gathering exp left to get to the next level</summary>
    public int gatheringExpLeft;

    public int getLevel(Skilltype type)
    {
        switch (type)
        {
            case Skilltype.attacking:
                return attacking;
            case Skilltype.cooking:
                return cooking;
            case Skilltype.crafting:
                return cooking;
            case Skilltype.defence:
                return defence;
            case Skilltype.farming:
                return farming;
            case Skilltype.fishing:
                return fishing;
            case Skilltype.mining:
                return mining;
            case Skilltype.trading:
                return trading;
            case Skilltype.gathering:
                return gathering;
            default:
                return 0;
        }
    }

    public void LevelSkills(int exp, Skilltype type)
    {
        switch (type)
        {
            case Skilltype.attacking:
                if (attackingExpLeft < 10000)
                    attackingExpLeft -= exp;
                while (attackingExpLeft <= 0)
                {
                    if (attacking >= expNeeded.Length) return;
                    attackingExpLeft += expNeeded[attacking];
                    Player.Main.AddMaxHealth(10);
                    attacking++;
                }
                break;
            case Skilltype.cooking:
                if (cookingExpLeft < 10000)
                    cookingExpLeft -= exp;
                while (cookingExpLeft <= 0)
                {
                    cookingExpLeft += expNeeded[cooking];
                    cooking++;
                }
                break;
            case Skilltype.crafting:
                if (craftingExpLeft < 10000)
                    craftingExpLeft -= exp;
                while (craftingExpLeft <= 0)
                {
                    craftingExpLeft += expNeeded[crafting];
                    crafting++;
                }
                break;
            case Skilltype.defence:
                if (defenceExpLeft < 10000)
                    defenceExpLeft -= exp;
                while (defenceExpLeft <= 0)
                {
                    defenceExpLeft += expNeeded[defence];
                    defence++;
                }
                break;
            case Skilltype.farming:
                if (farmingExpLeft < 10000)
                    farmingExpLeft -= exp;
                while (farmingExpLeft <= 0)
                {
                    farmingExpLeft += expNeeded[farming];
                    farming++;
                }
                break;
            case Skilltype.fishing:
                if (fishingExpLeft < 10000)
                    fishingExpLeft -= exp;
                while (fishingExpLeft <= 0)
                {
                    fishingExpLeft += expNeeded[fishing];
                    fishing++;
                }
                break;
            case Skilltype.mining:
                if (miningExpLeft < 10000)
                    miningExpLeft -= exp;
                while (miningExpLeft <= 0)
                {
                    miningExpLeft += expNeeded[mining];
                    mining++;
                }
                break;
            case Skilltype.trading:
                if (tradingExpLeft < 10000)
                    tradingExpLeft -= exp;
                while (tradingExpLeft <= 0)
                {
                    tradingExpLeft += expNeeded[trading];
                    trading++;
                }
                break;
        }
    }
}
