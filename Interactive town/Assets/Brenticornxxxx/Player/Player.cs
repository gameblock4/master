﻿using UnityEngine;
using System.Collections;
using Amphyra.Items;
using QRCoder;
using System.Collections.Generic;
using UnityEngine.Analytics;

namespace Amphyra.Actors
{
    public class Player : Actor
    {
        /// <summary>the posision the player is moving towards</summary>
        private Vector3 moveTarget;

        [SerializeField]
        Rect[] mouseClickBlacklist;

        [SerializeField]
        private bool showBlackListedArea;

        [SerializeField]
        private Camera miniMapCamera;

        private Coroutine gettingResource;
        [Header("Camera")]
        [SerializeField]
        private int minCameraDistance;
        [SerializeField]
        private int maxCameraDistance;
        [SerializeField]
        [Range(0, 20)]
        private float cameraZoomSpeed;
        [SerializeField]
        [Range(0, 20)]
        private float cameraMoveSpeed;

        [SerializeField]
        private Skills skills;

        [SerializeField]
        private float stamina = 100;

        [SerializeField]
        private GameObject cameraTarget;
        [SerializeField]
        private float cameraDistanceFromPlayer;
        [SerializeField]
        private int cameraRotationSpeed;

        /// <summary>name of the ground</summary>
        [SerializeField]
        private string groundTag;

        [SerializeField]
        LayerMask mask;

        private bool stopGettingResource;

        public static Player Main;

        private Vector3 respawnPosition;

        public bool isGamePaused;

        public MeshRenderer[] qrCodeHolders;

        public int damageBoost;

        public float Stamina
        {
            get { return stamina; }
        }

        public Texture2D QRImage;

        public GameObject shieldObject;
        public GameObject swordObject;


        public Player() : base(100, 0, Behavior.Neutral, 42, 24)
        {
            Main = this;
        }

        public override void Awake()
        {
            QR.GenerateQRCode();
            for (int i = 0; i < qrCodeHolders.Length; i++)
            {
                qrCodeHolders[i].material.mainTexture = QR.QRCodes[i].texture;
            }
            base.Awake();
            inventory = SaveAndLoad.Load<Inventory>("PlayerInventory", SaveAndLoad.Extention.txt);
            skills = SaveAndLoad.Load<Skills>("PlayerSkills", SaveAndLoad.Extention.txt);
            armor = SaveAndLoad.Load<ArmorInventory>("PlayerArmor", SaveAndLoad.Extention.txt);
            float[] pos = SaveAndLoad.Load<float[]>("PlayerPosition", SaveAndLoad.Extention.txt);
            if(pos != default(float[]))
            transform.position = new Vector3(pos[0], pos[1], pos[2]);

            float[] respawnPos = SaveAndLoad.Load<float[]>("PlayerRespawnPosition", SaveAndLoad.Extention.txt);
            if (pos != default(float[]))
                respawnPosition = new Vector3(pos[0], pos[1], pos[2]);

            cameraTarget.transform.parent.parent.position = transform.position;
            if (inventory == default(Inventory))
            {
                inventory = new Inventory(42, 24);
            }
            if (skills == default(Skills))
            {
                skills = new Skills();
            }
            if (armor == default(ArmorInventory))
            {
                armor = new ArmorInventory(0, 0, 0, 0, 0);
            }
            UpdateItems();
        }

        public override void Interact()
        {
        }

        public void CollectItem(int id, int amount)
        {
            if (inventory.CheckIfSpaceForItem(id, amount))
            {
                inventory.AddToInventory(id, amount);
                QuestManager.Collect(id, amount);
            }
        }

        public override bool Hit(int damage, Actor attacker, AttackType attackType)
        {
            int damagereduction = Mathf.RoundToInt(skills.defence * damage / (skills.expNeeded.Length * 2f));
            if (damagereduction > 0) print(damagereduction);
            skills.LevelSkills(1, Skilltype.defence);
            return base.Hit(damage - damagereduction, attacker, attackType);
        }

        public void UpdateItems()
        {
            Item swordItem = armor.GetHandItem();
            Texture2D swordTexture = null;
            if (swordItem != null)
            {
                swordTexture = IDManager.GetTexture(swordItem.Texture);

                if (swordTexture != null)
                    swordObject.GetComponent<Renderer>().material.mainTexture = swordTexture;
            }
            Item shieldItem = armor.GetShield();
            Texture2D shieldTexture = null;
            if (shieldItem != null)
            {
                shieldTexture = IDManager.GetTexture(shieldItem.Texture);

                if (shieldTexture != null)
                    shieldObject.GetComponent<Renderer>().material.mainTexture = shieldTexture;
            }
        }

        private void OnGUI()
        {
            if (!showBlackListedArea) return;
            for (int i = 0; i < mouseClickBlacklist.Length; i++)
            {
                Rect rect = mouseClickBlacklist[i];
                switch (i)
                {
                    case 2:
                        rect.x = Camera.main.pixelWidth - rect.x;
                        rect.y = Camera.main.pixelHeight - rect.y;
                        break;
                    case 1:
                        rect.y = Camera.main.pixelHeight - rect.y;
                        break;
                }

                GUI.Box(rect, "");
            }
        }

        private bool CheckIfMouseIsInBlackListArea(bool simulateClickOnMap)
        {
            for (int i = 0; i < mouseClickBlacklist.Length; i++)
            {
                Rect rect = mouseClickBlacklist[i];
                switch (i)
                {
                    case 2:
                        rect.x = Camera.main.pixelWidth - rect.x;
                        rect.y = Camera.main.pixelHeight - rect.y;
                        break;
                    case 1:
                        rect.y = Camera.main.pixelHeight - rect.y;
                        break;
                    default:
                        break;

                }
                Vector3 mousePos = Input.mousePosition;
                mousePos.y = Camera.main.pixelHeight - mousePos.y;
                if (mousePos.x > rect.x && mousePos.x < rect.x + rect.width)
                {
                    if (mousePos.y > rect.y && mousePos.y < rect.y + rect.height)
                    {
                        if (i == 1 && simulateClickOnMap)
                        {
                            Vector2 resolution = new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);
                            float posX = rect.width * Input.mousePosition.x / 10;
                            float posY = rect.height * Input.mousePosition.y / 10;
                            print(posX + "\n" + posY);
                            Ray ray = miniMapCamera.ScreenPointToRay(new Vector3(posX, posY));
                            RaycastHit hit;
                            if (Physics.Raycast(ray, out hit))
                            {
                                attackTarget = null;
                                if (hit.collider.gameObject.name == groundTag)
                                {
                                    moveTarget = new Vector3(hit.point.x, 1, hit.point.z);
                                    ParticleManager.InstantiateParticles(hit.point, ParticleType.movementClick);
                                    targetRotation = Quaternion.LookRotation(moveTarget - transform.position, Vector3.up);
                                    isMoving = true;
                                    stopGettingResource = true;
                                }
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        private void Update()
        {
            if (isGamePaused) return;
            if (Input.GetMouseButtonDown(0))
            {
                if (!CheckIfMouseIsInBlackListArea(true))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, mask))
                    {
                        attackTarget = null;
                        if (hit.collider.gameObject.tag == groundTag)
                        {
                            moveTarget = new Vector3(hit.point.x, 1, hit.point.z);
                            ParticleManager.InstantiateParticles(hit.point, ParticleType.movementClick);
                            targetRotation = Quaternion.LookRotation(moveTarget - transform.position, Vector3.up);
                            isMoving = true;
                            stopGettingResource = true;
                        }
                        else
                        {
                            Interacteble interactingScript = hit.collider.GetComponent<Interacteble>();
                            if (interactingScript)
                            {
                                moveTarget = interactingScript.transform.position;
                                interactingScript.Interact();
                                stopGettingResource = true;
                            }
                        }
                    }
                }
            }
            if (Input.GetMouseButtonDown(1))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Actor actorScript = hit.collider.GetComponent<Actor>();
                    if (actorScript)
                    {
                        if (actorScript.name != name)
                        {
                            moveTarget = new Vector3(hit.point.x, 1, hit.point.z);
                            StartAttacking(actorScript);
                            targetRotation = Quaternion.LookRotation(actorScript.transform.position - transform.position, Vector3.up);
                            isMoving = true;

                        }
                    }
                    Resource resourceScript = hit.collider.GetComponent<Resource>();
                    if (resourceScript)
                    {
                        if (gettingResource != null)
                            StopCoroutine(gettingResource);
                        if (resourceScript.GetObjectToDestroy())
                            gettingResource = StartCoroutine(GetResource(resourceScript));
                        moveTarget = new Vector3(hit.point.x, 1, hit.point.z);
                        isMoving = true;
                    }
                }
            }
            if (Input.GetMouseButton(2))
            {

                /*Vector3 target = cameraTarget.transform.parent.rotation.eulerAngles + ;
                if (target.x > 360)//if you go above 360 it goes back to 0 //target.x < minCameraX || 
                    target.x = cameraTarget.transform.parent.rotation.eulerAngles.x;

                target.x = cameraTarget.transform.parent.rotation.eulerAngles.x;
                */
                cameraTarget.transform.parent.parent.Rotate(new Vector3(0, 0, Input.GetAxis("Mouse X") * cameraRotationSpeed));

                cameraTarget.transform.parent.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * cameraRotationSpeed, 0, 0), Space.Self);
                //Vector3 xIs0 = cameraTarget.transform.parent.rotation.eulerAngles;
                //xIs0.x = 0;
                //cameraTarget.transform.parent.rotation = Quaternion.Euler(xIs0);
            }


            if (Input.GetAxis("Mouse ScrollWheel") > 0)//check if the the player if using the scrol wheel and the direction
                cameraDistanceFromPlayer -= cameraZoomSpeed;
            else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                cameraDistanceFromPlayer += cameraZoomSpeed;
            if (cameraDistanceFromPlayer < minCameraDistance)
                cameraDistanceFromPlayer = minCameraDistance;
            if (cameraDistanceFromPlayer > maxCameraDistance)
                cameraDistanceFromPlayer = maxCameraDistance;
        }

        public void FixedUpdate()
        {
            cameraTarget.transform.localPosition = Vector3.Lerp(cameraTarget.transform.localPosition, new Vector3(0, cameraDistanceFromPlayer, 0), Time.fixedDeltaTime * (cameraZoomSpeed / 2f));
            cameraTarget.transform.parent.parent.transform.position = Vector3.Lerp(cameraTarget.transform.parent.position, transform.position, Time.fixedDeltaTime * cameraMoveSpeed);

            if (moveTarget != transform.position && isMoving)
            {
                if (isAttacking || !stopGettingResource)
                    Move(moveTarget, targetRotation, 3.5f, Time.fixedDeltaTime);
                Move(moveTarget, targetRotation, 0, Time.fixedDeltaTime);
            }

        }

        private void RemoveStamina(float amount, Skilltype type)
        {
            stamina -= amount - (skills.getLevel(type) * amount / skills.expNeeded.Length * 2f);
        }

        public void AddStamina(float amount)
        {
            if (stamina + amount <= 100)
            {
                stamina += amount;
            }
            else stamina = 100;
        }

        public void AddHealth(int amount)
        {
            if (health + amount <= maxHealth)
            {
                health += amount;
            }
            else health = maxHealth;
        }

        public void AddMaxHealth(int amount)
        {
            maxHealth += amount;
        }

        void OnApplicationQuit()
        {
            Debug.Log("[Player] Application is exiting! saving inventory and skills at: " + SaveAndLoad.path);
            SaveAndLoad.Save(inventory, "PlayerInventory", SaveAndLoad.Extention.txt);
            SaveAndLoad.Save(skills, "PlayerSkills", SaveAndLoad.Extention.txt);
            SaveAndLoad.Save(armor, "PlayerArmor", SaveAndLoad.Extention.txt);
            float[] pos = new float[] { transform.position.x, transform.position.y, transform.position.z };
            SaveAndLoad.Save(pos, "PlayerPosition", SaveAndLoad.Extention.txt);
        }

        protected override IEnumerator Attack(Actor enemy, float range)
        {
            //set is attacking to true and store the enemy in this class
            isAttacking = true;
            attackTarget = enemy;

            //set it to the max value so it will allways go true it once
            float sqrDistance = int.MaxValue;

            //wait one second
            yield return new WaitForSeconds(1);
            //check if the enemy is null if so stop attacking(the enemy is probebly killed by someone else)
            if (enemy == null)
            {
                StopAttacking();
                yield break;
            }
            //while not in range wait and repeat unil in range
            while (sqrDistance > range * range)
            {
                yield return new WaitForSeconds(0.1f);
                if (enemy == null)
                {
                    StopAttacking();
                    yield break;
                }
                if ((transform.position - enemy.transform.position).sqrMagnitude > range * range * 1.3f)
                {
                    StopAttacking();
                    yield break;
                }
                sqrDistance = (transform.position - enemy.transform.position).sqrMagnitude;
            }
            //while the enemie has health
            while (enemy.Health > 0)
            {
                //check if the enemy is null if so stop attacking(the enemy is probebly killed by someone else)
                if (enemy == null)
                {
                    StopAttacking();
                    yield break;
                }
                //if out of range stop attacking
                if ((transform.position - enemy.transform.position).sqrMagnitude > range * range)
                {
                    StopAttacking();
                    yield break;
                }
                //set standard delay to 3 ,damage to 1 and the type to slashing
                float delay = 3;
                int damage = 1;
                AttackType type = AttackType.Slashing;

                //get the item class of the hand item
                Item item = armor.GetHandItem();

                //if the item is not null set the damage, attacktype and delay to the items damage, attacktype and delay
                if (item != null)
                {
                    if (item.Type == ItemType.AttackItem)
                    {
                        damage = (((AttackItem)item).Damage + Mathf.RoundToInt(skills.attacking * ((AttackItem)item).Damage / (skills.expNeeded.Length * 2f))) * (1 + damageBoost / 100);
                        type = ((AttackItem)item).AttackType;
                        delay = ((AttackItem)item).AttackDelay;
                        ((AttackItem)item).OnAttack(enemy, this);
                    }
                }
                //else if the item is not null debug a warning that the item with that id couldn't be found
                else if (armor.handItem != 0)
                {
                    print("[" + name + "] cant find item with id: " + armor.handItem);
                    StopAttacking();
                    yield break;
                }
                RemoveStamina(1, Skilltype.attacking);
                skills.LevelSkills(enemy.MaxHealth / 5, Skilltype.attacking);
                //damage the enemy
                if (enemy.Hit(damage, this, type))
                {
                    //if the enemy died check if it was a quest to kill it
                    QuestManager.Collect(enemy.name, 1);
                    skills.LevelSkills(enemy.MaxHealth / 2, Skilltype.attacking);
                }

                //tell the item it is attacking(he is a verry nice guy)
                if (item != null)
                    if (item.Type == ItemType.AttackItem)
                        ((AttackItem)item).OnAttack(enemy, this);

                RemoveStamina(1, Skilltype.attacking);

                //wait for the delay
                yield return new WaitForSeconds(delay);
            }
            isAttacking = false;
            attackTarget = null;
        }

        protected IEnumerator GetResource(Resource resource)
        {
            stopAttacking = true;
            stopGettingResource = false;

            while (Vector3.Distance(transform.position, resource.transform.position) > 5)
            {
                yield return new WaitForSeconds(0.2f);
            }

            float time = 2f;
            Item handItem = IDManager.GetItem(armor.handItem);
            if (handItem == null) yield break;
            if (handItem.Type != ItemType.Tool) yield break;
            if (((ToolItem)handItem).ToolType == resource.ToolType)
            {
                time = ((ToolItem)handItem).ResourceBreakTime;
            }
            else yield break;

            time -= skills.farming * time / (skills.expNeeded.Length * 2f);

            for (int i = 0; i < time; i++)
            {
                if (Vector3.Distance(transform.position, resource.transform.position) < 5 && !stopGettingResource)
                {
                    RemoveStamina(1, Skilltype.gathering);
                    ((ToolItem)handItem).OnUse(this, resource);
                    resource.OnHit(this);
                    if (time - i > 1)

                        yield return new WaitForSeconds(1);
                    else
                        yield return new WaitForSeconds(time - i);
                }
                else yield break;
            }
            skills.LevelSkills(1, Skilltype.gathering);

            int item = resource.CollectResource(this);
            if (inventory.CheckIfSpaceForItem(item, 1))
            {
                inventory.AddToInventory(item, 1);
            }
            stopGettingResource = true;
        }

        public override void Die()
        {
            transform.position = respawnPosition;
            health = maxHealth;
        }



    }
}




public enum Skilltype
{
    fishing,
    mining,
    cooking,
    farming,
    trading,
    attacking,
    defence,
    crafting,
    gathering,
}