﻿namespace Amphyra.Items
{
    /// <summary>
    /// These items can't be used unless its for a quest
    /// </summary>
    public class KeyItem : Item
    {
        public KeyItem(string name, int id, string textureName, int value)
            : base(name,id, ItemType.KeyItem, textureName, value)
        {

        }
    }
}
