﻿using Amphyra.Actors;

namespace Amphyra.Items
{
    /// <summary>
    /// Base class for Tools
    /// </summary>
    public class ToolItem : Item
    {
        /// <summary> How much time does it take to break a resource </summary>
        private float resourceBreakTime;

        /// <summary> How much time does it take to break a resource </summary>
        public float ResourceBreakTime
        {
            get { return resourceBreakTime; }
        }

        /// <summary>The type of this tool</summary>
        private ToolType toolType;

        /// <summary>The type of this tool</summary>
        public ToolType ToolType
        {
            get { return toolType; }
        }

        public ToolItem(string name, int id, string textureName, int value, float resourceBreakTime, ToolType toolType)
            : base(name, id, ItemType.Tool, textureName, value)
        {
            this.resourceBreakTime = resourceBreakTime;
            this.toolType = toolType;
        }

        /// <summary>
        /// this is called when the tool is used
        /// </summary>
        /// <param name="usingActor">who is using this item</param>
        /// <param name="collectedResource">the resouce getting collected</param>
        public virtual void OnUse(Actor usingActor, Resource collectedResource)
        {

        }
    }
}
public enum ToolType
{
    Pickaxe,
    Axe,
}