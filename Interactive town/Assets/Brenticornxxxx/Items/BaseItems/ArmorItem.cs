﻿namespace Amphyra.Items
{
    /// <summary>
    /// Base class for armor
    /// </summary>
    public class ArmorItem : Item
    {
        protected ArmorType armorType;
        /// <summary> The percentage of damage this armor will reduce. </summary>
        private int damageReductionPercentage;

        /// <summary> The percentage of damage this armor will reduce. </summary>
        public int DamageReductionPercentage
        {
            get { return damageReductionPercentage; }
        }

        /// <summary>what attacktype it protects from</summary>
        private AttackType protectType;

        /// <summary>what attacktype it protects from</summary>
        public AttackType ProtectType
        {
            get { return protectType; }
        }

        public ArmorType ArmorType
        {
            get { return armorType; }
        }

        public ArmorItem(string name, int id, string textureName, int value, int damageReductionPercentage, ArmorType type)
            : base(name, id, ItemType.ArmorItem, textureName, value, true, false)
        {
            armorType = type;
            this.damageReductionPercentage = damageReductionPercentage;
        }

        /// <summary>
        /// add a protect type
        /// </summary>
        /// <param name="type">type to protect from</param>
        protected void AddProtectType(AttackType type)
        {
            protectType = protectType | type;
        }

        /// <summary>
        /// check if its protecting from type type
        /// </summary>
        /// <param name="type">type to check</param>
        /// <returns></returns>
        public bool IsProtectingFrom(AttackType type)
        {
            return ((protectType & type) == type);
        }
    }
    [System.Serializable]
    public enum ArmorType
    {
        Helmet,
        Body,
        Leggings,
    }
}
