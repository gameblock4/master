﻿using Amphyra.Actors.Npcs;

namespace Amphyra.Items
{
    public class GiftItem : Item
    {
        public GiftItem(string name, int id, string textureName, int value)
            : base(name, id, ItemType.GiftItem, textureName, value)
        {

        }
        /// <summary>
        /// this is run when given to an npc
        /// </summary>
        /// <param name="from">from who</param>
        /// <param name="to">to who</param>
        public virtual void OnGiveToNPC(Npc from, Npc to)
        {
            UnityEngine.Debug.Log("gifting " + Name + " to " + to.name);
        }
    }
}