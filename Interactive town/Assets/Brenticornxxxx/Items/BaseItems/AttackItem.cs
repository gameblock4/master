﻿using Amphyra.Actors;

namespace Amphyra.Items
{
    /// <summary>
    /// Base class for weapons
    /// </summary>
    public class AttackItem : Item
    {
        /// <summary> How much damage does this item do when attacking </summary>
        private int damage;

        /// <summary>the delay between attacks</summary>
        private float attackDelay;

        /// <summary>the type of attack</summary>
        private AttackType attackType;

        /// <summary> How much damage does this item do when attacking </summary>
        public int Damage
        {
            get { return damage; }
        }

        /// <summary>the delay between attacks</summary>
        public float AttackDelay
        {
            get { return attackDelay; }
        }

        /// <summary>the type of attack</summary>
        public AttackType AttackType
        {
            get { return attackType; }
        }

        public AttackItem(string name, int id, string textureName, int value, int damage, float attackDelay, AttackType attackType)
            : base(name, id, ItemType.AttackItem, textureName, value)
        {
            this.damage = damage;
            this.attackDelay = attackDelay;
            this.attackType = attackType;
        }

        /// <summary>
        /// this is called when used to attack
        /// </summary>
        /// <param name="attackedActor">who is attacked</param>
        /// <param name="attackingActor">who is attacking</param>
        public virtual void OnAttack(Actor attackedActor, Actor attackingActor)
        {

        }
    }
}
