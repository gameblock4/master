﻿namespace Amphyra.Items
{
    public class AxeItem : ToolItem
    {
        public AxeItem(string name, int id, string textureName, int value, float resourceBreakTime) : base(name, id, textureName, value, resourceBreakTime, ToolType.Axe)
        {

        }
    }
}
