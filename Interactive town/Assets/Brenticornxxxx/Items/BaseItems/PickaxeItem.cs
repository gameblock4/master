﻿namespace Amphyra.Items
{
    public class PickaxeItem : ToolItem
    {
        public PickaxeItem(string name, int id, string textureName, int value, float resourceBreakTime) : base(name, id, textureName, value, resourceBreakTime, ToolType.Pickaxe)
        {

        }
    }
}
