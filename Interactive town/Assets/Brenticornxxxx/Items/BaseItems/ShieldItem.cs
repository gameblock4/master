﻿using Amphyra.Actors;

namespace Amphyra.Items
{
    /// <summary>
    /// base class for all shields
    /// </summary>
    public class ShieldItem : Item
    {
        /// <summary>percentage of damage to protect from</summary>
        private int reductionPercentage;

        /// <summary>percentage of damage to protect from</summary>
        public int ReductionPercentage
        {
            get { return reductionPercentage; }
        }

        /// <summary>attacktype to protect from</summary>
        private AttackType protectType;

        /// <summary>attacktype to protect from</summary>
        public AttackType ProtectType
        {
            get { return protectType; }
        }

        public ShieldItem(string name,int id, string textureName, int value, int reductionPercentage)
            : base(name, id, ItemType.ShieldItem, textureName, value)
        {
            this.reductionPercentage = reductionPercentage;
        }

        /// <summary>
        /// this is called when the item is used to attack
        /// </summary>
        /// <param name="attackActor">who is attacking</param>
        /// <param name="DefendActor">who is protecting</param>
        public virtual void OnBlock(Actor attackActor, Actor DefendActor)
        {

        }

        /// <summary>
        /// add a attacktype to protect from
        /// </summary>
        /// <param name="type">type to add</param>
        protected void AddProtectType(AttackType type)
        {
            protectType = protectType | type;
        }

        /// <summary>
        /// check if protecting from
        /// </summary>
        /// <param name="type">type to check</param>
        /// <returns></returns>
        public bool IsProtectingFrom(AttackType type)
        {
            return ((protectType & type) == type);
        }
    }
}