﻿using UnityEngine;
using System.Collections;

namespace Amphyra.Items
{
    public class Inventory
    {
        /// <summary>all items slots</summary>
        public ItemStack[] Slots;

        public bool[] bags;

        public int BagsCollected
        {
            get
            {
                int x = 0;
                for (int i = 0; i < bags.Length; i++)
                    if (bags[i])
                        x++;
                return x;
            }
        }

        /// <summary>how many slots have been unlocked</summary>
        public int unlockedSlots;

        public Inventory(int maxSlots, int unlockedSlots)
        {
            this.unlockedSlots = unlockedSlots;
            Slots = new ItemStack[maxSlots];
            for (int i = 0; i < Slots.Length; i++)
            {
                Slots[i] = new ItemStack();
            }
            bags = new bool[3];
        }

        /// <summary>
        /// check if there is space for an item
        /// </summary>
        /// <param name="item">id of the item you want to check</param>
        /// <param name="amount">amount you want to check for</param>
        /// <returns></returns>
        public bool CheckIfSpaceForItem(int item, int amount)
        {
            for (int i = 0; i < Slots.Length; i++)
            {
                //check if this slot contains the item or if its empty
                if (Slots[i].itemID == item || Slots[i].itemID == 0)
                {
                    //check if it has space
                    if (Slots[i].amount + amount < 100)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Check if the item is in the inventory
        /// </summary>
        /// <param name="item">The id of the item to check for</param>
        /// <param name="amount">how much to check for</param>
        /// <returns>return true if there are enough of that item</returns>
        public bool CheckForItem(int item, int amount = 1)
        {
            //the amount that still have to be checked
            int amountLeft = amount;

            for (int i = 0; i < Slots.Length; i++)
            {
                //check if this slot contains the item or if its empty
                if (Slots[i].itemID == item)
                {
                    //remove the amount of this slot from amountLeft
                    amountLeft -= Slots[i].amount;
                }
            }
            //if the amount has been found return true
            if (amountLeft <= 0)
                return true;

            //else return false
            return false;
        }

        /// <summary>
        /// add items to the inventory
        /// </summary>
        /// <param name="item">item to add</param>
        /// <param name="amount">amount to add</param>
        public void AddToInventory(int item, int amount)
        {
            //how many items are left
            int itemsleft = amount;
            //an item stack can never have more than 99
            if (amount > 99) return;
            //check all slots if there item id is the same is the id you are trying to add
            for (int i = 0; i < Slots.Length; i++)
            {
                if (Slots[i].itemID == item)
                {
                    //remove the items and check how many items cant fit
                    Slots[i].amount += itemsleft;
                    if (Slots[i].amount > 99)
                    {
                        itemsleft = Slots[i].amount - 99;
                        Slots[i].amount = 99;
                    }
                    else return;
                }
            }
            //if there are still items left check if there are empty slots and put them there
            for (int i = 0; i < Slots.Length; i++)
            {
                if (Slots[i].itemID == 0)
                {
                    Slots[i].itemID = item;
                    Slots[i].amount = itemsleft;
                    return;
                }
            }
        }

        /// <summary>
        /// removes the item and the amount and return the amount it couldn't remove
        /// </summary>
        /// <param name="item">Item to remove</param>
        /// <param name="amount">Amount to remove</param>
        /// <returns>returns the amount that it couldn't remove</returns>
        public int RemoveItems(int item, int amount)
        {
            //items left to remove
            int itemsLeft = amount;

            for (int i = 0; i < Slots.Length; i++)
            {
                //it the item from this slot is equel to the item its looking for
                if (Slots[i].itemID == item)
                {
                    //if the amount in the slot is more than we still have to find remove the amount we still have to remove and return 0
                    if(Slots[i].amount > itemsLeft)
                    {
                        Slots[i].amount -= itemsLeft;
                        return 0;
                    }
                    //else set the item and the amount to 0 and remove the amount from itemsLeft
                    else
                    {
                        itemsLeft -= Slots[i].amount;
                        Slots[i].amount = 0;
                        Slots[i].itemID = 0;
                    }
                }
                //if there are no items left break the for loop
                if (itemsLeft <= 0)
                {
                    itemsLeft = 0;
                    break;
                }
            }
            //return the itemsLeft
            return itemsLeft;
        }
    }
}
