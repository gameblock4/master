﻿namespace Amphyra.Items
{
    /// <summary>
    /// this contains all item types
    /// </summary>
    public enum ItemType
    {
        GiftItem,
        KeyItem,
        AttackItem,
        ArmorItem,
        ShieldItem,
        Normal,
        Tool,
    }
}