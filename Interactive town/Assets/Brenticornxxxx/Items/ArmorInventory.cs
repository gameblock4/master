﻿using Amphyra.Items;
using Amphyra.Actors;
using UnityEngine;

[System.Serializable]
public class ArmorInventory
{
    public int shield;
    public int helmet;
    public int body;
    public int legs;
    public int handItem;


    public ArmorInventory(int shield, int helmet, int body, int legs, int handItem)
    {
        this.shield = shield;
        this.helmet = helmet;
        this.body = body;
        this.legs = legs;
        this.handItem = handItem;
    }

    /// <summary>
    /// returns the old shield and sets a new one
    /// </summary>
    /// <param name="shieldID">the new shield</param>
    /// <returns>id of the old shield</returns>
    public int SetShield(int shieldID)
    {
        int currentShield = shield;
        shield = shieldID;
        Player.Main.UpdateItems();
        return currentShield;
    }

    /// <summary>
    /// returns the old helmet and sets a new one
    /// </summary>
    /// <param name="helmetID">the new helmet</param>
    /// <returns>id of the old helmet</returns>
    public int SetHelmet(int helmetID)
    {
        int currentHelmet = helmet;
        helmet = helmetID;
        Player.Main.UpdateItems();
        return currentHelmet;
    }

    /// <summary>
    /// returns the old body and sets a new one
    /// </summary>
    /// <param name="bodyID">the new body</param>
    /// <returns>id of the old body</returns>
    public int SetBody(int bodyID)
    {
        int currentBody = body;
        body = bodyID;
        Player.Main.UpdateItems();
        return currentBody;
    }

    /// <summary>
    /// returns the old legs and sets a new one
    /// </summary>
    /// <param name="legsID">the new legs</param>
    /// <returns>id of the old legs</returns>
    public int SetLegs(int legsID)
    {
        int currentLegs = legs;
        legs = legsID;
        Player.Main.UpdateItems();
        return currentLegs;
    }

    /// <summary>
    /// returns the old handItem and sets a new one
    /// </summary>
    /// <param name="handItemID">the new handItem</param>
    /// <returns>id of the old handItem</returns>
    public int SetHandItem(int handItemID)
    {
        int currentLegs = handItem;
        handItem = handItemID;
        Player.Main.UpdateItems();
        return currentLegs;
    }

    /// <summary>
    /// returns the item class of the current shield
    /// </summary>
    /// <returns>shield class</returns>
    public Item GetShield()
    {
        return IDManager.GetItem(shield);
    }

    /// <summary>
    /// returns the item class of the current helmet
    /// </summary>
    /// <returns>helmet class</returns>
    public Item GetHelmet()
    {
        return IDManager.GetItem(helmet);
    }

    /// <summary>
    /// returns the item class of the current body
    /// </summary>
    /// <returns>body class</returns>
    public Item GetBody()
    {
        return IDManager.GetItem(body);
    }

    /// <summary>
    /// returns the item class of the current legs
    /// </summary>
    /// <returns>legs class</returns>
    public Item GetLegs()
    {
        return IDManager.GetItem(legs);
    }

    /// <summary>
    /// returns the item class of the current handItem
    /// </summary>
    /// <returns>handItem class</returns>
    public Item GetHandItem()
    {
        return IDManager.GetItem(handItem);
    }
}