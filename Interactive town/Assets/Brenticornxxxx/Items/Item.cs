﻿using System;
using Amphyra.Actors;

namespace Amphyra.Items
{
    public class Item
    {
        private int id;
        private string name;
        private ItemType type;
        private string texture;
        private int value;
        private bool isLoot;
        private bool stackeble;

        /// <summary>ID of the Item. </summary>
        public virtual int ID
        {
            get { return id; }
        }
        /// <summary>Name of the item. </summary>
        public virtual string Name
        {
            get { return name; }
        }
        /// <summary>Item type. </summary>
        public virtual ItemType Type
        {
            get { return type; }
        }
        /// <summary>UI Texture of the item. </summary>
        public virtual string Texture
        {
            get { return texture; }
        }
        /// <summary>Item price </summary>
        public virtual int Value
        {
            get { return value; }
        }

        /// <summary>Item price </summary>
        public virtual bool IsLoot
        {
            get { return isLoot; }
        }

        /// <summary>Can this item stack </summary>
        public virtual bool Stackeble
        {
            get { return stackeble; }
        }

        public Item(string name, int id, ItemType type, string texture, int value)
        {
            this.id = id;
            this.name = name;
            this.type = type;
            this.texture = texture;
            this.value = value;
            isLoot = false;
            stackeble = false;
            IDManager.Register(this);
        }

        public Item(string name, int id, ItemType type, string texture, int value, bool isLoot, bool stackeble)
        {
            this.id = id;
            this.name = name;
            this.type = type;
            this.texture = texture;
            this.value = value;
            this.isLoot = isLoot;
            this.stackeble = stackeble;
            IDManager.Register(this);
        }

        /// <summary>This is called every second</summary>
        public virtual void Update()
        {

        }

        /// <summary>
        /// This gets called when the item is being used for crafting
        /// </summary>
        /// <param name="player"><see cref="Actor"/> of the player</param>
        /// <param name="craftedItem">the <see cref="Item"/> that got crafted</param>
        public virtual void OnUsedForCrafting(Actor player, Item craftedItem)
        {

        }
        /// <summary>
        /// This gets called when the item is being crafted
        /// </summary>
        /// <param name="player"><see cref="Actor"/> of the Player</param>
        public virtual void Oncrafted(Actor player)
        {

        }
    }
}
