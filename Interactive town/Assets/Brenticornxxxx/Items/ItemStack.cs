﻿namespace Amphyra.Items
{
    /// <summary>
    /// itemstack
    /// </summary>
    [System.Serializable]
    public class ItemStack
    {
        public int itemID;
        public int amount;

        public ItemStack(int itemID, int amount)
        {
            this.itemID = itemID;
            this.amount = amount;
        }

        public ItemStack() { }
    }
}
