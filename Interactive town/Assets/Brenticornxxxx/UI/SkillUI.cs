﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amphyra.Actors;

namespace Amphyra.SkillUI
{
    public class SkillUI : MonoBehaviour
    {
        [SerializeField]
        private Button[] skillBtns;
        [SerializeField]
        private Image[] coolDownImages;
        private Timer[] skillTimer;
        public static float timeReduction = 1;
        private Timer healTimer;
        [SerializeField]
        private int healthAdded = 20;
        private Timer damageBoostTimer;

        // Use this for initialization
        void Start()
        {
            skillTimer = new Timer[skillBtns.Length];
            healTimer = new Timer();
            damageBoostTimer = new Timer();

            for (int i = 0; i < skillBtns.Length; i++)
            {
                skillTimer[i] = new Timer();
                coolDownImages[i].gameObject.SetActive(false);

            }


        }

        // Update is called once per frame
        void Update()
        {

            for (int i = 0; i < skillBtns.Length; i++)
            {
                coolDownImages[i].fillAmount = (1 - skillTimer[i].TimerProgress());

                if (skillTimer[i].TimerDone())
                {
                    coolDownImages[i].gameObject.SetActive(false);
                }
            }
            if (healthAdded < 20)
            {
                healTimer.SetTimer(1);
                Player.Main.AddHealth(1);
                healthAdded++;
            }

            if (!damageBoostTimer.TimerDone())
            {
                Player.Main.damageBoost = 25;
            }
            else
            {
                Player.Main.damageBoost = 0;
            }
        }

        public void TimeReduce(int newTimeReduce)
        {
            timeReduction = newTimeReduce;
        }

        public void ActivateSkills(int skillUsed)
        {
            switch (skillUsed)
            {

                case 0:
                    if (skillTimer[0].TimerDone())
                    {
                        HealSkill();
                        coolDownImages[0].gameObject.SetActive(true);
                        skillTimer[0].SetTimer(15 * timeReduction);
                    }
                    break;
                case 1:
                    if (skillTimer[1].TimerDone())
                    {
                        BoostDamage();
                        coolDownImages[1].gameObject.SetActive(true);
                        skillTimer[1].SetTimer(45 * timeReduction);
                    }
                    break;
                case 2:
                    if (skillTimer[2].TimerDone())
                    {
                        Rest();
                        coolDownImages[2].gameObject.SetActive(true);
                        skillTimer[2].SetTimer(60 * timeReduction);
                    }
                    break;
                default:
                    break;
            }
        }

        private void HealSkill()
        {
            Debug.Log("Add Health");

            if (healthAdded == 20)
            {
                healthAdded = 0;
            }
        }

        private void BoostDamage()
        {
            damageBoostTimer.SetTimer(6);
            Debug.Log("Damage boost for 6 seconds of 25%");

        }

        private void Rest()
        {
            Debug.Log("can be used twice after that it wont have any effect, regens 10% of stamina");
            Player.Main.AddStamina(10);
        }

        private void FocussedSmash()
        {
            Debug.Log("Next hit deals 150% damage");
        }
    }
}
