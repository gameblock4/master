﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{

    AsyncOperation LevelLoader;

    public GameObject startScreen;
    public GameObject loadingScreen;

    public Image loadingImage;

    public float lerpSpeedMultiplier;

    public void ChangeScene()
    {
        //start loading a scene
        LevelLoader = SceneManager.LoadSceneAsync(1);
        //prevent the scene from activating
        LevelLoader.allowSceneActivation = false;
        //deactivate the current canvas and activate the progress canvas
        startScreen.SetActive(false);
        loadingScreen.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    void FixedUpdate()
    {
        //if there is no scene Loading return
        if (LevelLoader == null) return;
        //the progressbar fill amount is set to a lerp between the currentFillAmount and the loading progress with a "t" as the time between updates
        loadingImage.fillAmount = Mathf.Lerp(loadingImage.fillAmount, LevelLoader.progress + 0.1f, Time.fixedDeltaTime * lerpSpeedMultiplier);
        //if the fillamount is > 99 activate the scene
        if (loadingImage.fillAmount > 0.99f)
        {
            LevelLoader.allowSceneActivation = true;
        }
    }
}
