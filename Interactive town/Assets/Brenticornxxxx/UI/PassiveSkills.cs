﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amphyra.Actors;
using Amphyra.SkillUI;

public class PassiveSkills : MonoBehaviour {

	private Image[] passiveSkills;
	private Player player;
	private SkillUI skillCooldown;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (player.Health < player.MaxHealth * 0.5F) {
			DefenceBoost (true);
		} else {
			DefenceBoost(false);
		}
	}

	private void DefenceBoost(bool activated) {
		if (activated == true) {
			//damage reduction by 20%
			passiveSkills[3].gameObject.SetActive(true);
		}
	}
}
