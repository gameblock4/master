﻿using Amphyra.Actors;
using Amphyra.Items;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    public Image[] buttons;
    public Image[] items;
    public Image[] bags;
    static Color clearColor;

    public Image mouseSlot;
    public Image[] armorSlots;

    int mouseSlotItem;
    int mouseSlotAmount;
    static Actor lastActor;

    private static InventoryUI main;

    public void Start()
    {
        main = this;
        lastActor = Player.Main;
        if (main.gameObject.activeSelf)
        {
            lastActor = Player.Main;
            for (int i = 0; i < main.bags.Length; i++)
            {
                UpdateBag(i);
            }
            for (int i = 0; i < main.buttons.Length; i++)
            {
                main.UpdateInventorySlot(i);
            }
            for (int i = 0; i < main.armorSlots.Length; i++)
            {
                main.UpdateArmorSlot(i);
            }
            main.UpdateMouseSlot();
        }
        OpenOrCloseInventory(null);
    }

    public void Update()
    {
        if (main.gameObject.activeSelf)
        {
            mouseSlot.rectTransform.position = Input.mousePosition + new Vector3(mouseSlot.rectTransform.rect.width / 2, -mouseSlot.rectTransform.rect.height / 2);
        }
    }

    public void CloseInventory()
    {
        OpenOrCloseInventory(null);
    }

    public void OpenInventory(Actor actor)
    {
        OpenOrCloseInventory(actor);
    }

    public static void OpenOrCloseInventory(Actor actor)
    {
        if (actor == null)
            main.gameObject.SetActive(false);
        else
            main.gameObject.SetActive(!main.gameObject.activeSelf);
        Player.Main.isGamePaused = main.gameObject.activeSelf;
        
    }

    public void OnArmorSlotClicked(int type)
    {
        Item item = IDManager.GetItem(mouseSlotItem);
        if (item == null)
        {
            switch (type)
            {
                case 0:
                    mouseSlotItem = lastActor.Armor.SetHelmet(0);
                    break;
                case 1:
                    mouseSlotItem = lastActor.Armor.SetBody(0);
                    break;
                case 2:
                    mouseSlotItem = lastActor.Armor.SetLegs(0);
                    break;
                case 3:
                    mouseSlotItem = lastActor.Armor.SetHandItem(0);
                    break;
                case 4:
                    mouseSlotItem = lastActor.Armor.SetShield(0);
                    break;
            }
            mouseSlotAmount = 1;
            UpdateArmorSlot(type);
            UpdateMouseSlot();
            return;
        }
        switch (type)
        {
            case 0:
                if(item.Type == ItemType.ArmorItem)
                if (((ArmorItem)item).ArmorType == ArmorType.Helmet)
                {
                    mouseSlotItem = lastActor.Armor.SetHelmet(mouseSlotItem);
                    mouseSlotAmount = ((mouseSlotItem != 0 ? 1 : 0));
                }
                break;
            case 1:
                if (item.Type == ItemType.ArmorItem)
                    if (((ArmorItem)item).ArmorType == ArmorType.Body)
                {
                    mouseSlotItem = lastActor.Armor.SetBody(mouseSlotItem);
                    mouseSlotAmount = ((mouseSlotItem != 0 ? 1 : 0));
                }
                break;
            case 2:
                if (item.Type == ItemType.ArmorItem)
                    if (((ArmorItem)item).ArmorType == ArmorType.Leggings)
                {
                    mouseSlotItem = lastActor.Armor.SetLegs(mouseSlotItem);
                    mouseSlotAmount = ((mouseSlotItem != 0 ? 1 : 0));
                }
                break;
            case 3:
                if (item.Type == ItemType.AttackItem || item.Type == ItemType.Tool)
                {
                    mouseSlotItem = lastActor.Armor.SetHandItem(mouseSlotItem);
                    mouseSlotAmount = ((mouseSlotItem != 0 ? 1 : 0));
                }
                break;
            case 4:
                if (item.Type == ItemType.ShieldItem)
                {
                    mouseSlotItem = lastActor.Armor.SetShield(mouseSlotItem);
                    mouseSlotAmount = ((mouseSlotItem != 0 ? 1 : 0));
                }
                break;
        }
        UpdateArmorSlot(type);
        UpdateMouseSlot();
    }

    /// <summary>
    /// move items from the mouse inventory with the bag that is clicked
    /// </summary>
    /// <param name="slot">what bag is clicked</param>
    public void OnBagSlotClick(int slot)
    {
        if (mouseSlotItem == 0 && lastActor.inventory.bags[slot])
        {
            lastActor.inventory.bags[slot] = false;
            mouseSlotItem = 19;
            mouseSlotAmount = 1;
        }
        else if (mouseSlotItem == 19 && mouseSlotAmount > 0 && !lastActor.inventory.bags[slot])
        {
            lastActor.inventory.bags[slot] = true;
            mouseSlotItem = 0;
            mouseSlotAmount = 0;
        }
        UpdateBag(slot);
        UpdateMouseSlot();
    }

    /// <summary>
    /// this is for the item slot buttons in the ui. this will move the items with the mouse
    /// </summary>
    /// <param name="slot"></param>
    public void OnSlotClick(int slot)
    {
        //when the mouse has items
        if (mouseSlotItem != 0 && mouseSlotAmount > 0)
        {
            //when the item from the mouse is not the same as the item from the slot swich the items and amount
            if (mouseSlotItem != lastActor.inventory.Slots[slot].itemID)
            {
                int mouseItem = mouseSlotItem;
                int mouseAmount = mouseSlotAmount;
                mouseSlotItem = lastActor.inventory.Slots[slot].itemID;
                mouseSlotAmount = lastActor.inventory.Slots[slot].amount;
                lastActor.inventory.Slots[slot].itemID = mouseItem;
                lastActor.inventory.Slots[slot].amount = mouseAmount;
            }
            //when the item is the same move as much as possible from the mouse to the item slot
            else
            {
                //if the item can be stacked
                if (IDManager.GetItem(lastActor.inventory.Slots[slot].itemID).Stackeble)
                {
                    //if the slot amount + the mouse slot amount is less than 100 move all items to that slot
                    if (lastActor.inventory.Slots[slot].amount + mouseSlotAmount < 100)
                    {
                        lastActor.inventory.Slots[slot].amount += mouseSlotAmount;
                        mouseSlotAmount = 0;
                        mouseSlotItem = 0;
                    }
                    //else move as much as possible to that slot and keep the rest in the mouse slot
                    else
                    {
                        int itemsToMove = 99 - lastActor.inventory.Slots[slot].amount;
                        lastActor.inventory.Slots[slot].amount = 99;
                        mouseSlotAmount -= itemsToMove;
                    }
                }
            }
        }
        //if the slot is empty
        else if (lastActor.inventory.Slots[slot].itemID == 0)
        {
            //if the mouse slot is not empty
            if (mouseSlotItem != 0)
            {
                //put the items from the mouse to the slot
                lastActor.inventory.Slots[slot].itemID = mouseSlotItem;
                lastActor.inventory.Slots[slot].amount = mouseSlotAmount;
            }
        }
        //if the mouse is empty but the item slot not move the items from the item slot to the mouse slot
        else if (mouseSlotItem == 0 && lastActor.inventory.Slots[slot].itemID != 0)
        {
            mouseSlotItem = lastActor.inventory.Slots[slot].itemID;
            mouseSlotAmount = lastActor.inventory.Slots[slot].amount;
            lastActor.inventory.Slots[slot].amount = 0;
            lastActor.inventory.Slots[slot].itemID = 0;
        }

        //update the ui
        UpdateInventorySlot(slot);
        UpdateMouseSlot();
    }

    /// <summary>
    /// updates the texture and text of the bag inside the ui
    /// </summary>
    /// <param name="bag">which bag to update</param>
    public static void UpdateBag(int bag)
    {
        if (lastActor.inventory.bags[bag])
        {
            Texture2D itemTexture = IDManager.GetTexture("ItemBag");
            Rect rect = new Rect(0, 0, itemTexture.width, itemTexture.height);
            main.bags[bag].color = Color.white;
            main.bags[bag].sprite = Sprite.Create(itemTexture, rect, new Vector2(0.5f, 0.5f));
            main.bags[bag].transform.parent.GetChild(0).GetComponent<Text>().text = "Bag placed";
        }
        else
        {
            main.bags[bag].color = Color.clear;
            main.bags[bag].sprite = null;
            main.bags[bag].transform.parent.GetChild(0).GetComponent<Text>().text = "No bag";
        }
        for (int i = 24 + ((lastActor.inventory.BagsCollected - 1) * 6); i < main.buttons.Length; i++)
        {
            main.UpdateInventorySlot(i);
        }
    }

    /// <summary>
    /// updates the texture and text of the inventory slot inside the ui
    /// </summary>
    /// <param name="slot">which slot to update</param>
    public void UpdateInventorySlot(int slot)
    {

        if (slot < lastActor.inventory.unlockedSlots + (lastActor.inventory.BagsCollected * 6))
        {
            main.items[slot].color = Color.white;
            main.buttons[slot].GetComponent<Button>().interactable = true;
            Item item = IDManager.GetItem(lastActor.inventory.Slots[slot].itemID);
            if (item != null)
            {
                Texture2D itemTexture = IDManager.GetTexture(item.Texture);
                Rect rect = new Rect(0, 0, itemTexture.width, itemTexture.height);
                main.items[slot].color = Color.white;
                main.items[slot].sprite = Sprite.Create(itemTexture, rect, new Vector2(0.5f, 0.5f));
                main.buttons[slot].rectTransform.GetChild(0).GetComponent<Text>().text = item.Name + ": " + lastActor.inventory.Slots[slot].amount;
            }
            else
            {
                main.items[slot].color = Color.clear;
                main.items[slot].sprite = null;
                main.buttons[slot].rectTransform.GetChild(0).GetComponent<Text>().text = string.Empty;
            }
        }
        else
        {
            main.buttons[slot].GetComponent<Button>().interactable = false;
            main.buttons[slot].rectTransform.GetChild(0).GetComponent<Text>().text = string.Empty;
            main.items[slot].color = Color.clear;
        }

    }

    /// <summary>
    /// updates the texture and text of the mouse slot inside the ui
    /// </summary>
    public void UpdateMouseSlot()
    {
        if (main.mouseSlotItem != 0)
        {
            if (!main.mouseSlot.gameObject.activeSelf)
            {
                main.mouseSlot.gameObject.SetActive(true);
            }
            main.mouseSlot.color = Color.white;
            Item mouseItem = IDManager.GetItem(main.mouseSlotItem);
            if (mouseItem != null)
            {
                Texture2D itemtexture = IDManager.GetTexture(mouseItem.Texture);
                Rect rect = new Rect(0, 0, itemtexture.width, itemtexture.height);
                main.mouseSlot.rectTransform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create(itemtexture, rect, new Vector2(0.5f, 0.5f));
                main.mouseSlot.rectTransform.GetChild(0).GetComponent<Text>().text = mouseItem.Name + ": " + main.mouseSlotAmount;
            }
            else
            {
                main.mouseSlot.rectTransform.GetChild(1).GetComponent<Image>().sprite = null;
                main.mouseSlot.rectTransform.GetChild(0).GetComponent<Text>().text = string.Empty;
            }
        }
        else
        {
            main.mouseSlot.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// updates the texture and text of the armor slot inside the ui
    /// </summary>
    /// <param name="slot">which slot to update</param>
    public void UpdateArmorSlot(int slot)
    {
        Item mouseItem = null;
        switch (slot)
        {
            case 0:
                mouseItem = lastActor.Armor.GetHelmet();
                break;
            case 1:
                mouseItem = lastActor.Armor.GetBody();
                break;
            case 2:
                mouseItem = lastActor.Armor.GetLegs();
                break;
            case 3:
                mouseItem = lastActor.Armor.GetHandItem();
                break;
            case 4:
                mouseItem = lastActor.Armor.GetShield();
                break;
        }
        if (mouseItem != null)
        {
            Texture2D itemtexture = IDManager.GetTexture(mouseItem.Texture);
            Rect rect = new Rect(0, 0, itemtexture.width, itemtexture.height);
            main.armorSlots[slot].rectTransform.GetChild(1).GetComponent<Image>().color = Color.white;
            main.armorSlots[slot].rectTransform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create(itemtexture, rect, new Vector2(0.5f, 0.5f));
            main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = mouseItem.Name;
        }
        else
        {
            main.armorSlots[slot].rectTransform.GetChild(1).GetComponent<Image>().color = Color.clear;
            main.armorSlots[slot].rectTransform.GetChild(1).GetComponent<Image>().sprite = null;
            switch (slot)
            {
                case 0:
                    main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = "No helmet";
                    break;
                case 1:
                    main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = "No body armor";
                    break;
                case 2:
                    main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = "No leggings";
                    break;
                case 3:
                    main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = "No hand item";
                    break;
                case 4:
                    main.armorSlots[slot].rectTransform.GetChild(0).GetComponent<Text>().text = "No shield";
                    break;
            }
        }
    }
}