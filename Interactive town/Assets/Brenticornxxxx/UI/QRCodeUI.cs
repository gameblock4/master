﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amphyra.Actors;

public class QRCodeUI : MonoBehaviour
{
    private Image image;

    [SerializeField]
    private InputField input;

    public char[] letters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
    public char[] numbers = "0123456789".ToCharArray();

    public Text[] textButtons;
    public Text[] numberButtons;

    public void Awake()
    {
        image = transform.GetChild(0).GetComponent<Image>();
        Close();
        for (int i = 0; i < textButtons.Length; i++)
        {
            textButtons[i].text = letters[i].ToString();
        }
        for (int i = 0; i < numberButtons.Length; i++)
        {
            numberButtons[i].text = numbers[i].ToString();
        }
    }

    public void SwichLowerUpper()
    {
        if (textButtons[0].text == letters[0].ToString().ToLower())
        {
            ToUpper();
        }
        else ToLower();
    }

    private void ToLower()
    {
        for (int i = 0; i < textButtons.Length; i++)
        {
            textButtons[i].text = letters[i].ToString().ToLower();
        }
    }

    private void ToUpper()
    {
        for (int i = 0; i < textButtons.Length; i++)
        {
            textButtons[i].text = letters[i].ToString().ToUpper();
        }
    }

    public void BackSpace()
    {
        if (input.text.Length > 0)
            input.text = input.text.Remove(input.text.Length - 1);
        Check(input.text);
    }

    public void Check(string code)
    {
        if (QR.ControlQRCode(code))
        {
            image.color = Color.white;
            image.sprite = IDManager.GetTexture("QrCode" + QR.GetQRcodeIndex(code)).ToSprite();
        }
        else
        {
            image.color = Color.clear;
            image.sprite = null;
        }
    }

    public void Open()
    {
        InventoryUI.OpenOrCloseInventory(null);
        transform.GetChild(1).gameObject.SetActive(true);
        transform.GetChild(2).gameObject.SetActive(true);
        Player.Main.isGamePaused = true;
    }

    public void Close()
    {
        input.text = "";
        Player.Main.isGamePaused = false;
        image.color = Color.clear;
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
    }

    public void Input(int i)
    {
        if (i < letters.Length)
        {
            input.text += textButtons[i].text;
        }
        else
        {
            input.text += numberButtons[i - letters.Length].text;
        }
        Check(input.text);
    }
}
