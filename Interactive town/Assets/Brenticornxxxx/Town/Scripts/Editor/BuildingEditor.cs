﻿using UnityEngine;
using UnityEditor;
using Amphyra.Town;

[CustomEditor(typeof(Building),true)]
public class BuildingEditor : Editor
{
    Building building;
    void OnEnable()
    {
        building = ((Building)target);
    }

    public void OnSceneGUI()
    {
        EditorGUI.BeginChangeCheck();
        Vector3 exitPosision = Handles.PositionHandle(building.buildingExit + building.transform.position, Quaternion.identity);

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Changed exit posision");
            building.buildingExit = exitPosision - building.transform.position;
        }
    }
}
