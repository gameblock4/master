﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amphyra.Actors.Npcs;

namespace Amphyra.Town
{
    public class TownManager : MonoBehaviour
    {
        public List<Building> buildings;

        public static TownManager main;

        public TownManager()
        {
            main = this;
        }

        public void Start()
        {
            StartCoroutine(UpdateBuildings());
        }

        public Vector3 GetClosestBuilding(Npc npc, BuildingType type)
        {
            float closestdistance = int.MaxValue;
            int best = -1;
            for (int i = 0; i < buildings.Count; i++)
            {
                if (buildings[i].BuildingType == type)
                {
                    float distence = Vector3.Distance(npc.transform.position, buildings[i].transform.position);
                    if (distence < closestdistance)
                    {
                        closestdistance = distence;
                        best = i;
                    }
                }
            }
            if (best == -1)
            {
                Debug.Log(name);
                return Vector3.zero;
            }
                return buildings[best].transform.position;
        }

        public int Register(Building building)
        {
            buildings.Add(building);
            return buildings.Count - 1;
        }

        public IEnumerator UpdateBuildings()
        {
            while(5* Time.deltaTime < 1)
            {
                for (int i = 0; i < buildings.Count; i++)
                {
                    buildings[i].BuildingUpdate();
                    yield return new WaitForSeconds((10f / buildings.Count));
                }
            }
            Debug.Log("Lagg spike! Stopping townActivitie");
            yield return new WaitForSeconds(20);
            StartCoroutine(UpdateBuildings());
        }
    }
}