﻿
using Amphyra.Actors.Npcs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Amphyra.Town
{
    public class Building : Interacteble
    {
        [SerializeField]
        protected Vector2 size;

        public Vector3 buildingExit = new Vector3(0, 0, 0);

        [SerializeField]
        protected BuildingData buildingData;

        public BuildingData BuildingData
        {
            get { return buildingData; }
        }

        [SerializeField]
        protected int ID;

        [SerializeField]
        protected List<int> NPCsInside;

        [SerializeField]
        protected BuildingType buildingType;

        public BuildingType BuildingType
        {
            get { return buildingType; }
        }

        public void Awake()
        {
            buildingData = GetStandardBuildingData();
            ID = TownManager.main.Register(this);
        }

        public virtual void BuildingUpdate()
        {
        }

        public virtual int GetBestNpc(JobEnum job = JobEnum.UnEmployed)
        {
            for (int i = 0; i < NPCsInside.Count; i++)
            {
                Npc npc = IDManager.GetNpc(NPCsInside[i]);
                if (npc)
                {
                    if (npc.NpcData.job == job)
                    {
                        return NPCsInside[i];
                    }
                }
            }
            return -1;
        }

        public override void Interact()
        {

        }

        public virtual void EnterBuilding(Npc npc)
        {
            NPCsInside.Add(npc.ID);
        }

        public void ExitBuilding(int npcID)
        {
            for (int i = 0; i < NPCsInside.Count; i++)
            {
                if (NPCsInside[i] == npcID)
                {
                    NPCsInside.RemoveAt(i);
                }
            }
        }

        protected static BuildingData GetStandardBuildingData()
        {
            BuildingData data = new BuildingData();
            data.food = 250;
            return data;
        }
    }
}
[Serializable]
public class BuildingData
{
    public int food;
}