﻿[System.Serializable]
public enum BuildingType
{
    House,
    Work,
    Mission,
    TownBuilding,
    Shop,
}