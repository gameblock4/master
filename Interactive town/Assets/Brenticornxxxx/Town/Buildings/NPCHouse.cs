﻿using UnityEngine;
using System.Collections;
using Amphyra.Actors.Npcs;

namespace Amphyra.Town
{
    public class NPCHouse : Building
    {
        public bool isNpcGettingFood = false;
        public override void BuildingUpdate()
        {
            base.BuildingUpdate();
            buildingData.food -= NPCsInside.Count;
            if (buildingData.food < 50 && isNpcGettingFood == false)
            {
                int bestnpc = GetBestNpc();
                Npc npc = IDManager.GetNpc(bestnpc);
                print("I need food");
                if (npc != null)
                {
                    print(bestnpc + " is getting food");
                    isNpcGettingFood = true;
                    npc.stateMachine.ChangeState(new NpcGoShoppingState(npc.stateMachine, this));
                }
            }
        }
    }
}