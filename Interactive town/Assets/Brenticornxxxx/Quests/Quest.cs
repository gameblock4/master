﻿using UnityEngine;
using System.Collections;
using Amphyra.Items;
using Amphyra.Actors;
using System;

[Serializable]
public class Quest
{
    public int amount;

    public int itemIDToCollect;

    public string ActorToKill;

    public int itemReward;
    public int rewardAmount;

    public void removeAmount(int amount)
    {
        this.amount -= amount;
    }
}