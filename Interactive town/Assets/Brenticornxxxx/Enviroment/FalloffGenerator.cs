﻿using UnityEngine;
using System.Collections;

public static class FalloffGenerator
{

    static float a = 1.3f;
    static float b = 3;
    /// <summary>
    /// generate a falloff map for the waves so they dont flood the town
    /// </summary>
    /// <param name="size">the size of the map</param>
    /// <returns>Float[,]</returns>
    public static float[,] GenerateFalloffMap(int size)
    {
        float[,] map = new float[size, size];
        //loops between all floats and get the max of x and y and evaluats it
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                //evaluate value
                map[i, j] = Evaluate(Mathf.Max(Mathf.Abs(i / (float)size * 2 - 1), Mathf.Abs(j / (float)size * 2 - 1)));
            }
        }

        return map;
    }

    /// <summary>creats a square with border glow</summary>
    /// <param name="value">the value</param>
    /// <returns>(value ^ a) / (value ^ a) + ((b - b * value) ^ a)</returns>
    static float Evaluate(float value)
    {
        return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
    }
}