﻿using UnityEngine;
using System.Collections;
using System;
using Amphyra.Actors;

public class CuttebleTree : Resource
{
    //if one of these is true it means that axis wil get a random rotation
    public bool x;
    public bool y;
    public bool z;

    //if true the gameObject wil move to the ground using a raycast
    [SerializeField]
    private bool setToGround;

    //posision from the hit point of the raycast
    public Vector3 posisionFromRayHit;

    public CuttebleTree() : base(12, ToolType.Axe)
    {

    }

    public void Awake()
    {
        #region set the rotations
        int xRot = 0;
        if (x)
        {
            xRot = UnityEngine.Random.Range(0, 360);
        }
        int yRot = 0;
        if (y)
        {
            yRot = UnityEngine.Random.Range(0, 360);
        }
        int zRot = 0;
        if (z)
        {
            zRot = UnityEngine.Random.Range(0, 360);
        }

        transform.rotation = Quaternion.Euler(new Vector3(xRot, yRot, zRot));
        #endregion

        //if setToGround is true then raycast down and move to the hit posision
        if (setToGround)
        {
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                transform.position = hit.point + posisionFromRayHit;
            }
        }
    }

    /// <summary>
    /// when this gets hit with an axe
    /// </summary>
    /// <param name="actor">who is chopping down this mighty tree</param>
    public override void OnHit(Actor actor)
    {
        base.OnHit(actor);
        ParticleManager.InstantiateParticles(transform.position + Vector3.up, ParticleType.specialEffect, 1);
    }

    /// <summary>
    /// destroy the tree and return the resource id
    /// </summary>
    /// <param name="actor"></param>
    /// <returns></returns>
    public override int CollectResource(Actor actor)
    {
        ParticleManager.InstantiateParticles(transform.position + Vector3.up, ParticleType.specialEffect, 1);
        return base.CollectResource(actor);
    }
}
