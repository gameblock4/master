﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class WaterMesh : MonoBehaviour
{
    public float waveScale;

    [Range(0, 500)]
    public float waveSize;

    [Range(0, 50)]
    public float waveSpeed;
    public Vector2 textureSpeed;


    [SerializeField]
    private int size;

    private Mesh mesh;
    private SkinnedMeshRenderer meshRenderer;

    private Vector3[] vertecies;
    private Vector2[] uvs;
    private int[] targetPos;
    private float[,] falloffMap;

    void Start()
    {
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
        vertecies = new Vector3[size * size];
        uvs = new Vector2[vertecies.Length];
        mesh = new Mesh();
        mesh.Clear();
        falloffMap = FalloffGenerator.GenerateFalloffMap(size);
        List<int> triangles = new List<int>();
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                vertecies[y + x * size] = new Vector3(x - size / 2, 0, y - size / 2);
                uvs[y + x * size] = new Vector2(x, y);
            }
        }
        for (int x = 0; x < size - 1; x++)
        {
            for (int y = 0; y < size - 1; y++)
            {
                triangles.Add(x * size + y);
                triangles.Add(x * size + y + size);
                triangles.Add(x * size + y + 1);
                triangles.Add(x * size + y + 1);
                triangles.Add(x * size + y + size);
                triangles.Add(x * size + y + size + 1);
            }
        }
        mesh.vertices = vertecies;
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs;
        mesh.MarkDynamic();
        meshRenderer.sharedMesh = mesh;
    }

    void FixedUpdate()
    {
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                vertecies[x * size + y].y = ((Mathf.PerlinNoise((x + Time.time * waveSpeed) * waveScale, (y + Time.time * waveSpeed) * waveScale) * 2 - 1) * (falloffMap[x, y] / 3)) * waveSize;
            }
        }
        mesh.vertices = vertecies;
        meshRenderer.material.mainTextureOffset += textureSpeed * Time.deltaTime;
    }
}
