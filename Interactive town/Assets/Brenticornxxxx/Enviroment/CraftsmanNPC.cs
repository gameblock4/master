﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Amphyra.Actors;

public class CraftsmanNPC : Interacteble
{
    [SerializeField]
    private Image CraftContainer;

    private void Start()
    {
        CraftContainer.gameObject.SetActive(false);

    }

    public override void Interact()
    {
        OpenShop();
        Player.Main.isGamePaused = true;

    }



    private void OpenShop()
    {
        CraftContainer.gameObject.SetActive(true);
    }

    public void CloseShop()
    {
        CraftContainer.gameObject.SetActive(false);
    }
}
