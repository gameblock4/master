﻿using UnityEngine;
using Amphyra.Actors;

public class Resource : Interacteble
{
    //is this the object that stays
    [SerializeField]
    private bool isBase;

    /// <summary>item that gets collected</summary>
    [SerializeField]
    protected int resource;

    /// <summary>effective tool</summary>
    protected ToolType toolType;

    /// <summary>effective tool</summary>
    public ToolType ToolType
    {
        get { return toolType; }
    }
    
    public Resource(int resource, ToolType toolType)
    {
        this.resource = resource;
        this.toolType = toolType;
    }

    public override void Interact()
    {

    }

    /// <summary>returns the object that has to bee destroyed when collected</summary>
    public virtual GameObject GetObjectToDestroy()
    {
        if (!isBase)
        {
            return gameObject;
        }
        else if (transform.childCount > 0) return transform.GetChild(0).gameObject;
        else return null;
    }

    /// <summary>
    /// destoy the resource and return the item
    /// </summary>
    /// <param name="actor">who collected the resource</param>
    public virtual int CollectResource(Actor actor)
    {
        if (GetObjectToDestroy().activeSelf)
        {
            GetObjectToDestroy().SetActive(false);
            return resource;
        }
        return 0;
    }

    public virtual void OnHit(Actor actor)
    {

    }
}
