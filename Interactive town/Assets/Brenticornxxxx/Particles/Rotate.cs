﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{

    [SerializeField]
    private float speed;
    [SerializeField]
    Vector3 startRotation;
    private void Start()
    {
        transform.localRotation = Quaternion.Euler(startRotation);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, speed);
    }
}
