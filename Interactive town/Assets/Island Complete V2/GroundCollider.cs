﻿using UnityEngine;
using System.Collections;

public class GroundCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;
	}
}
